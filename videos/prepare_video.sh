#!/bin/bash

if [ $# -lt 7 ]
then
    echo "Usage: $0 input_file n_tile_x n_tile_y encoder segment_duration baseURL target_bitrate [target_bitrate ...]"
    exit 1
fi

inputname=$1
basename=${inputname##*/}
basename=${basename%.*}
n_tile_x=$2
n_tile_y=$3
encoder=$4
keyframe_ms=$5
baseURL=$6
bitrates="${@:7}"
bitrates=${bitrates^^}

#converts bitrates literals like 2K into 2048 etc
#scales bitrates for each tile
for b in $bitrates
do
    b=$(numfmt --from=iec $b)
    if ! [[ $b =~ $re ]]
    then
        echo "invalid bitrate value $b"
        exit 1
    fi
    b=$((b / (n_tile_x * n_tile_y)))
    bitrates_full="$bitrates_full $b"
done

mkdir -p $basename

#tiles input video and encodes them for each bitrate
./tile.sh $n_tile_x $n_tile_y $inputname $basename $encoder $keyframe_ms $bitrates_full

#dashes all tiles and bitrates
size=$(ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 $inputname)
./dash.sh $basename $keyframe_ms "seg" ${basename}_size_${size}_tiled_${n_tile_x}_${n_tile_y} $baseURL

#rm reencoded files
rm ${basename}/*bitrate_*_tile_*_*.mp4
