#!/bin/bash

if [ $# -lt 4 ]
then
    echo "Usage: $0 n_tile_x n_tile_y video_input video_output_dir encoder segment_duration_ms target_bitrate [target_bitrate ...]"
    echo "Tiles a video in NxM video tiles using ffmpeg"
    exit 1
fi

n_tile_x=$1
n_tile_y=$2
inputname=$3
outputdir=$4
encoder=$5
keyframe_ms=$6
bitrates="${@:7}"

re='^[0-9]+$'

if ! [[ $n_tile_x =~ $re ]]
then
    echo "$n_tile_x is not a valid integer"
    exit 1
fi

if ! [[ $n_tile_y =~ $re ]]
then
    echo "$n_tile_y is not a valid integer"
    exit 1
fi

if [[ ! -f $inputname ]]
then
    echo "$inputname no such file"
    exit 1
fi

if ! [[ $encoder -eq "x264" || $encoder -eq "x265" ]]
then
    echo "encoder $encoder is not supported, correct values are x264, x265"
    exit 1
fi

if ! [[ $keyframe_ms =~ $re ]]
then
    echo "$keyframe_ms incorrect keyframe interval value"
    exit 1
fi

#compute input video size,fps and tile sizes
size=$(ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 $inputname)
fps=$(ffprobe -v error -select_streams v:0 -show_entries stream=r_frame_rate -of csv=s=x:p=0 $inputname)
w=$(echo $size | cut -d"x" -f1)
h=$(echo $size | cut -d"x" -f2)
tile_w=$((w / n_tile_x))
tile_h=$((h / n_tile_y))

#height and width have to be divisible by 2
if [ $((tile_h % 2)) -ne 0 ]
then
    tile_h=$((tile_h + 1))
fi

if [ $((tile_w % 2)) -ne 0 ]
then
    tile_w=$((tile_w + 1))
fi

keyframe_frames=$((($fps * $keyframe_ms) / 2000))

#uses ffmpeg with crop filter to tile the input video
for b in $bitrates
do
    bh=$(numfmt --to=iec $b)
    echo "bitrate $bh..."
    out="${outputdir}/bitrate_${bh}"
    ffmpeg="ffmpeg -y -i $inputname -filter_complex "
    out_map=""
    y=0
    for i in $(seq 1 $n_tile_y)
    do
	    x=0
        for j in $(seq 1 $n_tile_x)
        do
            tile_num=$((j - 1 + (i - 1) * n_tile_x))
            ffmpeg=${ffmpeg}"[0:v]crop=$tile_w:$tile_h:$x:$y[out${tile_num}];"
            out_map=${out_map}" -map [out${tile_num}]
                                -an
                                -r $fps
                                -s ${tile_w}x${tile_h}
                                -c:v lib${encoder}
                                -${encoder}-params keyint=${keyframe_frames}:min-keyint=${keyframe_frames}
                                -b:v $b
                                -maxrate $b
                                -bufsize "$((b * 2))"
                                -pix_fmt yuv420p
                                ${out}_tile_${i}_${j}.mp4"
            x=$((x + tile_w))
        done
        y=$((y + tile_h))
    done
    
    ffmpeg=${ffmpeg::-1}
    ffmpeg=${ffmpeg}${out_map}
    $ffmpeg
done
