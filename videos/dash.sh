#! /bin/bash

if [ $# -lt 4 ]
then
    echo "Usage: $0 input_directory duration_ms output_directory mpd_name base_URL"
    echo "Provide input directory with all your tiles and bitrates, a segment duration in ms, a directory to store fragments, a name for mpd file and a base url eg http://test.com/"
    exit 1
fi

indir=$1
indir=${indir%/}
seg_dur=$2
outdir=$3
outdir=${outdir%/}
mpd_name=$4
baseURL=$5

if [[ ! -d $indir ]]
then
    echo "$indir no such directory"
    exit 1
fi

re='^[0-9]+$'
if ! [[ $seg_dur =~ $re ]]
then
    echo "$seg_dur is not a valid segment duration"
    exit 1
fi

$(mkdir -p $outdir)
mpd_out_name="$indir/$mpd_name.mpd"

mp4box_command="MP4Box -dash $seg_dur
                       -frag $seg_dur
                       -rap
                       -url-template
                       -segment-name ${outdir}/\$RepresentationID\$_\$Number\$
                       -base-url $baseURL
                       -out $mpd_out_name"

for f in ${indir}/*bitrate_*_tile_*_*.mp4
do
    quality=${f##*/}
    quality=${quality%.*}
    mp4box_command=$mp4box_command" "$f":id="$quality
done

$mp4box_command


