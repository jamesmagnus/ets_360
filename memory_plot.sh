#!/bin/bash

if [ $# -lt 1 ]
then
    echo "Usage: " $0 " COMMAND"
    exit 1
fi

mprof clean &&
mprof run --multiprocess --interval 0.001 "$@" &&
mprof plot

