# - Try to find LibDASH
# Once done this will define
#  LIBDASH_FOUND - System has LibDASH
#  LIBDASH_INCLUDE_DIRS - The LibDASH include directories
#  LIBDASH_LIBRARIES - The libraries needed to use LibDASH

find_package(PkgConfig)
pkg_check_modules(PC_LIBDASH QUIET libdash)

SET(LIBDASH_SEARCH_PATHS
	~/Library/Frameworks
	/Library/Frameworks
	/usr/local
	/usr
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	${WIN_DEVLIB_DIR}
)

IF(CMAKE_SIZEOF_VOID_P EQUAL 8)
	set(LIBVER 64)
ELSE()
	set(LIBVER 86)
ENDIF()

find_path(LIBDASH_INCLUDE_DIR libdash.h
          HINTS ${PC_LIBDASH_INCLUDEDIR} ${PC_LIBDASH_INCLUDE_DIRS}
          PATH_SUFFIXES libdash
						dash
						include
						libdash/include
						dash/include
		  PATHS ${LIBDASH_SEARCH_PATHS})

find_library(LIBDASH_LIBRARY dash libdash
             HINTS ${PC_LIBDASH_LIBDIR} ${PC_LIBDASH_LIBRARY_DIRS}
             PATH_SUFFIXES libdash
						   dash
						   lib
						   bin
						   libdash/lib
						   dash/lib
						   libdash/bin
						   dash/bin
						   libdash/libx${LIBVER}
						   dash/libx${LIBVER}
						   libdash/binx${LIBVER}
						   dash/binx${LIBVER}
						   libx${LIBVER}
						   binx${LIBVER}
						   libdash/lib${LIBVER}
						   dash/lib${LIBVER}
						   libdash/bin${LIBVER}
						   dash/bin${LIBVER}
						   lib${LIBVER}
						   bin${LIBVER}
						   
			 PATHS ${LIBDASH_SEARCH_PATHS})
			 
find_library(LIBDASH_LIBRARY_DEBUG dashd libdashd
             HINTS ${PC_LIBDASH_LIBDIR} ${PC_LIBDASH_LIBRARY_DIRS}
             PATH_SUFFIXES libdash
						   dash
						   lib
						   bin
						   libdash/lib
						   dash/lib
						   libdash/bin
						   dash/bin
						   libdash/libx${LIBVER}
						   dash/libx${LIBVER}
						   libdash/binx${LIBVER}
						   dash/binx${LIBVER}
						   libx${LIBVER}
						   binx${LIBVER}
						   libdash/lib${LIBVER}
						   dash/lib${LIBVER}
						   libdash/bin${LIBVER}
						   dash/bin${LIBVER}
						   lib${LIBVER}
						   bin${LIBVER}
						   
			 PATHS ${LIBDASH_SEARCH_PATHS})

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(LibDASH  DEFAULT_MSG
                                  LIBDASH_LIBRARY LIBDASH_INCLUDE_DIR)

mark_as_advanced(LIBDASH_INCLUDE_DIR LIBDASH_LIBRARY )

set(LIBDASH_LIBRARIES ${LIBDASH_LIBRARY} )
set(LIBDASH_INCLUDE_DIRS ${LIBDASH_INCLUDE_DIR} )
