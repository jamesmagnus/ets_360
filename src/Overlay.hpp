/**
 * @file Overlay.hpp
 * @author Jean-Loup Beaussart
 * @class Overlay
 * @brief A abstract overlay base class.
 *
 * This class defines an screen overlay for SDL window.
 * It takes care of all the rendering stuff, derived classes have to setup the data they want to display.
 * They can add labels and for each label they should add a string representing the value to display.
 */

#pragma once
#include "SDLOverlay.hpp"
#include <map>
#include <string>
#include <vector>

class Overlay {
private:
    SDL::Overlay mOverlaySys;
    std::map<std::string, std::stringstream> mOverlayDataValues;
    std::vector<std::string> mOverlayDataLabels;

protected:
    std::map<std::string, std::stringstream> &dataValues();
    std::vector<std::string> &dataLabels();

public:
    Overlay(const Overlay &) = delete;
    Overlay &operator=(const Overlay &) = delete;
    Overlay(Overlay &&)                 = delete;
    Overlay &operator=(Overlay &&) = delete;

    explicit Overlay(std::unique_ptr<SDL::System> &pSDLSys);

    virtual ~Overlay() = default;

    virtual void setOverlayData() = 0;

    void updateOverlay();
};
