#define LOGURU_IMPLEMENTATION 1

#include "AudioDecoder.hpp"
#include "Clock.hpp"
#include "ConcurrentQueue.hpp"
#include "DASHAdaptNormal.hpp"
#include "DASHAdaptTiled.hpp"
#include "DASHReader.hpp"
#include "LoguruLogger.hpp"
#include "MediaReader.hpp"
#include "NamedPipe.hpp"
#include "PlayerOverlay.hpp"
#include "SDLSystem.hpp"
#include "Sync.hpp"
#include "ThreadRegistry.hpp"
#include "VideoData.hpp"
#include "VideoDecoder.hpp"
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <clocale>
#include <iostream>
#include <regex>

bool parseCommandLineOptions(int argc, char *argv[], VideoData &videoData);
bool analyseMediaFile(std::experimental::filesystem::path const &input, std::vector<std::string> &out, VideoData &videoData);
void createPipes(std::vector<std::string> &pipeNames, std::vector<std::reference_wrapper<NamedPipe<unsigned char>>> &pipes);
void createVideoQueues(uint64_t nbQueues, std::vector<std::unique_ptr<ConcurrentQueue<Packet>>> &pPacketQueues, std::vector<std::unique_ptr<ConcurrentQueue<YUV>>> &pRawQueues);
void createMediaReaders(uint64_t nbReaders, const std::vector<std::string> &files, std::vector<std::unique_ptr<ConcurrentQueue<Packet>>> &pOutVideoQs, std::unique_ptr<ConcurrentQueue<Packet>> &pOutAudioQ, std::vector<std::reference_wrapper<MediaReader>> &pReaders, const VideoData &videoData);
void initLog(int argc, char *argv[]);

template <class T>
void waitForElemInQs(const std::vector<std::unique_ptr<ConcurrentQueue<T>>> &pQs, uint64_t min)
{
    bool wait = true;

    while (wait)
    {
        wait = false;
        for (auto &q : pQs)
            if (q->nbElements() < min)
            {
                wait = true;
                break;
            }
    }
}

bool parseCommandLineOptions(int argc, char *argv[], VideoData &videoData)
{
    namespace po = boost::program_options;

    bool showHelp = false;
    po::options_description visible("Allowed options");
    po::options_description hidden;
    po::options_description all;
    po::positional_options_description positionalOptions;
    po::variables_map vm;

    visible.add_options()("help,h", "produce help message")("version,v", "print version number")("360", "the video is a 360 degrees video")("3D", "the video is a 3D video")("tiles,t", po::value<std::string>(), "the video is tiled in a 'NxM' grid")("hwaccel,a", "try to use hardware acceleration if available");
    hidden.add_options()("input-file", po::value<std::string>()->required(), "media file to play");
    all.add(visible);
    all.add(hidden);

    positionalOptions.add("input-file", 1);

    try
    {
        po::store(po::command_line_parser(argc, argv).options(all).positional(positionalOptions).run(), vm);

        if (vm.count("help") != 0)
            showHelp = true;
        if (vm.count("version") != 0)
        {
            std::cout << "Ver. 0.2" << std::endl;
            return false;
        }

        po::notify(vm);
    }
    catch (std::exception const &e)
    {
        if (!showHelp)
        {
            std::cerr << e.what() << std::endl;
            showHelp = true;
        }
    }

    videoData.path = vm["input-file"].as<std::string>();

    if (vm.count("360") != 0)
    {
        videoData.is360 = true;
        LOG_S(INFO) << "360 video";
    }

    if (vm.count("3D") != 0)
    {
        videoData.is3D = true;
        LOG_S(INFO) << "3D video";
    }

    if (vm.count("hwaccel") != 0)
    {
        videoData.useHwAccel = true;
        LOG_S(INFO) << "use hardware acceleration";
    }

    if (vm.count("tiles") != 0)
    {
        auto tileNumber = vm["tiles"].as<std::string>();
        std::regex tileRegex("^([1-9][0-9]*)x([1-9][0-9]*)$", std::regex::ECMAScript);
        std::cmatch m;
        if (!std::regex_match(tileNumber.c_str(), m, tileRegex))
        {
            std::cout << "option tiles expects 'NxM' argument" << std::endl;
            showHelp = true;
        }
        else
        {
            videoData.tileX = std::stoi(m.str(1));
            videoData.tileY = std::stoi(m.str(2));
            LOG_S(INFO) << "Tiled video (" << videoData.tileX << "x" << videoData.tileY << ")";
        }
    }

    if (showHelp)
    {
        std::cout << visible << std::endl;
        return false;
    }

    return true;
}

bool analyseMediaFile(std::experimental::filesystem::path const &input, std::vector<std::string> &out, VideoData &videoData)
{
    namespace fs = std::experimental::filesystem;

    fs::path extension = input.extension();
    CHECK_S(!extension.empty()) << input.string() << " cannot detect media type, no file extension";

    if (extension != ".mpd")
    {
        out.resize(1);
        out[0] = input.string();
        return false;
    }

    std::regex tiledVideo(".*size_([0-9]*)x([0-9]*)_tiled_([0-9]*)_([0-9]*).*", std::regex_constants::ECMAScript);
    std::cmatch cm;
    if (std::regex_match(input.string().c_str(), cm, tiledVideo))
    {
        videoData.w     = std::stoi(cm.str(1));
        videoData.h     = std::stoi(cm.str(2));
        videoData.tileX = std::stoi(cm.str(3));
        videoData.tileY = std::stoi(cm.str(4));
    }

    for (int i = 0, nbTiles = videoData.tileX * videoData.tileY; i < nbTiles; ++i)
        out.emplace_back(boost::filesystem::unique_path().string());

    return true;
}

void createPipes(std::vector<std::string> &pipeNames, std::vector<std::reference_wrapper<NamedPipe<unsigned char>>> &pipes)
{
    for_each(pipeNames.begin(), pipeNames.end(), [&pipes](auto &name) {
        auto &pipe = ThreadRegistry::get().newThread<NamedPipe<unsigned char>>("pipe");
        name       = pipe.create(name);
        pipes.emplace_back(pipe);
    });

    CHECK_S(pipeNames.size() == pipes.size());
}

void createVideoQueues(uint64_t nbQueues, std::vector<std::unique_ptr<ConcurrentQueue<Packet>>> &pPacketQueues, std::vector<std::unique_ptr<ConcurrentQueue<YUV>>> &pRawQueues)
{
    for (uint64_t i = 0; i < nbQueues; ++i)
    {
        auto pVideoPacketQ = std::make_unique<ConcurrentQueue<Packet>>();
        auto pRawVideoQ    = std::make_unique<ConcurrentQueue<YUV>>();

        pPacketQueues.push_back(std::move(pVideoPacketQ));
        pRawQueues.push_back(std::move(pRawVideoQ));
    }
}

void createMediaReaders(uint64_t nbReaders, const std::vector<std::string> &files, std::vector<std::unique_ptr<ConcurrentQueue<Packet>>> &pOutVideoQs, std::unique_ptr<ConcurrentQueue<Packet>> &pOutAudioQ, std::vector<std::reference_wrapper<MediaReader>> &pReaders, const VideoData &videoData)
{
    for (uint64_t i = 0; i < nbReaders; ++i)
    {
        auto &reader = ThreadRegistry::get().newThread<MediaReader>("reader", files[i], pOutVideoQs[i], pOutAudioQ, videoData);
        pReaders.emplace_back(reader);
    }
}

void initLog(int argc, char *argv[])
{
    loguru::init(argc, argv, nullptr);
    loguru::set_fatal_handler([](const loguru::Message &message) {
        throw std::runtime_error(std::string(message.prefix) + message.message);
    });
    loguru::add_file("player.log", loguru::Append, loguru::Verbosity_INFO);
    LOG_S(INFO) << "Log system initialized";
}

int main(int argc, char *argv[])
{
    VideoData videoData;

    /* Initialization */
    initLog(argc, argv);

    if (!parseCommandLineOptions(argc, argv, videoData))
        return EXIT_FAILURE;

    std::vector<std::reference_wrapper<NamedPipe<unsigned char>>> pipes;
    auto pDashReader        = std::make_unique<DASHReader>();
    DASHAdapt *pDASHAdapter = nullptr;
    std::vector<std::string> fileNames;
    auto pClock = std::make_shared<Clock>();

    // if DASH
    if (analyseMediaFile(videoData.path, fileNames, videoData))
    {
        LOG_S(INFO) << "DASH content detected";
        createPipes(fileNames, pipes);
        pDashReader->open(videoData.path);

        ThreadRegistry &tr = ThreadRegistry::get();
        if (videoData.tileX > 1 || videoData.tileY > 1)
            pDASHAdapter = &tr.newThread<DASHAdaptTiled>("dash", pipes, videoData);
        else
            pDASHAdapter = &tr.newThread<DASHAdaptNormal>("dash", pipes);
        pDASHAdapter->setReader(std::move(pDashReader));
        pDASHAdapter->attachClock(pClock);
        pDASHAdapter->startThread();
    }

    /*  Audio packet and raw queues */
    auto pAudioPacketQ = std::make_unique<ConcurrentQueue<Packet>>();
    auto pRawAudioQ    = std::make_unique<ConcurrentQueue<Sample>>();
    /* Video packet and raw queues */
    std::vector<std::unique_ptr<ConcurrentQueue<Packet>>> pVideoPacketQs;
    std::vector<std::unique_ptr<ConcurrentQueue<YUV>>> pRawVideoQs;

    createVideoQueues(videoData.nbTiles(), pVideoPacketQs, pRawVideoQs);

    /* Multimedia readers */
    std::vector<std::reference_wrapper<MediaReader>> readers;
    createMediaReaders(videoData.nbTiles(), fileNames, pVideoPacketQs, pAudioPacketQ, readers, videoData);

    // set video size from file reader if size wasn't set by DASH detection
    auto &firstReader = readers[0].get();
    if (videoData.w == 0)
        videoData.w = firstReader.videoWidth();
    if (videoData.h == 0)
        videoData.h = firstReader.videoHeight();

    /* SDL render system, overlay and sync clock */
    pClock->setVideoTimebase(firstReader.videoTimebase());
    pClock->setAudioTimebase(firstReader.audioTimebase());
    auto pRenderSystem                = std::make_unique<SDL::System>(SDL_INIT_VIDEO | SDL_INIT_AUDIO, "Player", videoData.w, videoData.h);
    std::unique_ptr<Overlay> pOverlay = std::make_unique<PlayerOverlay>(pRenderSystem, pClock, pVideoPacketQs[0], pAudioPacketQ, pRawVideoQs, pRawAudioQ);
    pRenderSystem->setOverlay(std::move(pOverlay));
    auto &synchronizer = ThreadRegistry::get().newThread<Sync>("sync", pRenderSystem, pRawVideoQs, pRawAudioQ, videoData);
    synchronizer.attachPlaybackClock(pClock);

    /* Create audio and video decoders */
    for (uint64_t i = 0, nbTiles = videoData.nbTiles(); i < nbTiles; ++i)
    {
        auto &reader = readers[i].get();
        reader.attachPlaybackClock(pClock);
        reader.attachVideoDecoder(ThreadRegistry::get().newThread<VideoDecoder>("decoder", reader.videoCtx(), pVideoPacketQs[i], pRawVideoQs[i]));
    }

    if (firstReader.hasAudio())    // if audio is present in DASH tiled video assume it is attached to first tile
    {
        pRenderSystem->setupAudio(firstReader.audioCtx()->sample_rate);
        firstReader.attachAudioDecoder(ThreadRegistry::get().newThread<AudioDecoder>("decoder", firstReader.audioCtx(), pAudioPacketQ, pRawAudioQ));
    }

    /* Start file reader threads */
    for (MediaReader &reader : readers)
        reader.startThread();

    waitForElemInQs(pRawVideoQs, 5);    // wait for buffers to fill up
    pClock->reset();                    // reset clock before starting display

    if (firstReader.hasAudio())
        synchronizer.startThread();

    synchronizer.updateLoop();    // SDL display and event loop

    LOG_S(INFO) << "Playback end";

    /* Uninitialization */
    for (NamedPipe<unsigned char> &pipe : pipes)
        pipe.emptyPipe();
    ThreadRegistry::get().joinAll();

    return EXIT_SUCCESS;
}
