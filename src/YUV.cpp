#include "YUV.hpp"
#include <array>

extern "C" {
#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable : 4244)
#endif

#include <libavutil/imgutils.h>

#ifdef _WIN32
#pragma warning(pop)
#endif
}

static void destroyImageData(std::array<unsigned char *, 4> *p)
{
    av_freep(&p->data()[0]);
    delete p;
}

YUV::YUV(int w, int h, AVPixelFormat pixFmt, int align, int64_t pts)
    : mppManagedData(new std::array<unsigned char *, 4>, destroyImageData),
      mcPTS(pts)
{
    int ret = av_image_alloc(data(), mpLinesize, w, h, pixFmt, align);

    if (ret < 0)
        throw std::bad_alloc();

    mSize = static_cast<size_t>(ret);
}

unsigned char *YUV::getPlan(uint32_t plan) const
{
    return data()[plan];
}

int YUV::getLinesize(uint32_t plan) const
{
    return mpLinesize[plan];
}

unsigned char **YUV::data() const
{
    return mppManagedData->data();
}

unsigned char *YUV::YData() const
{
    return getPlan(scY);
}

unsigned char *YUV::UData() const
{
    return getPlan(scU);
}

unsigned char *YUV::VData() const
{
    return getPlan(scV);
}

int const *YUV::linesize() const
{
    return mpLinesize;
}

int YUV::YSize() const
{
    return getLinesize(scY);
}

int YUV::USize() const
{
    return getLinesize(scU);
}

int YUV::VSize() const
{
    return getLinesize(scV);
}

size_t YUV::size() const
{
    return mSize;
}

int64_t YUV::pts() const
{
    return mcPTS;
}
