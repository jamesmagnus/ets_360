/**
 * @file PlayerOverlay.hpp
 * @author Jean-Loup Beaussart
 * @class PlayerOverlay
 * @brief An implementation of Overlay that displays playback time and queue sizes
 *
 */

#pragma once

#include "Overlay.hpp"
#include "Packet.hpp"
#include "Sample.hpp"
#include "YUV.hpp"
#include <vector>

class Clock;
template <class T>
class ConcurrentQueue;

class PlayerOverlay : public Overlay {
private:
    std::shared_ptr<const Clock> mpcClock;
    std::unique_ptr<ConcurrentQueue<Packet>> &mpInputVideoQ;
    std::unique_ptr<ConcurrentQueue<Packet>> &mpInputAudioQ;
    std::vector<std::unique_ptr<ConcurrentQueue<YUV>>> &mpVideoQs;
    std::unique_ptr<ConcurrentQueue<Sample>> &mpSampleQ;

public:
    PlayerOverlay(const PlayerOverlay &) = delete;
    PlayerOverlay &operator=(const PlayerOverlay &) = delete;
    PlayerOverlay(PlayerOverlay &&)                 = delete;
    PlayerOverlay &operator=(PlayerOverlay &&) = delete;

    PlayerOverlay(std::unique_ptr<SDL::System> &pSDLSys, std::shared_ptr<const Clock> pcClock, std::unique_ptr<ConcurrentQueue<Packet>> &pInputVideoQ, std::unique_ptr<ConcurrentQueue<Packet>> &pInputAudioQ, std::vector<std::unique_ptr<ConcurrentQueue<YUV>>> &pVideoQs, std::unique_ptr<ConcurrentQueue<Sample>> &pSampleQ);

    ~PlayerOverlay() override = default;

    void setOverlayData() override;
};
