/**
 * @file ConcurrentQueue.hpp
 * @author Jean-Loup Beaussart
 * @class ConcurrentQueue
 * @brief Implements a template concurrent queue.
 *
 * This queue uses a mutex and a condition variable in order to be thread-safe.
 * Calls to get() sleep the calling thread if queue is empty, thread is waken up by a call to tryPut() from another thread or by flushing the queue.
 * @tparam T is the type of objects in the queue. Data is not copied, the queue just hold an owning pointer to it.
 */
#pragma once

#include "LoguruLogger.hpp"
#include <condition_variable>
#include <queue>

template <class T>
class ConcurrentQueue {
private:
    std::queue<std::unique_ptr<T>> mQ;
    std::condition_variable mCondOut;
    mutable std::mutex mMutex;
    size_t mSize{0};
    bool mFlushed{false};

public:
    /**
     * @brief Deleted copy constructor.
     */
    ConcurrentQueue(const ConcurrentQueue &) = delete;

    /**
     * @brief Deleted assignment operator.
     */
    ConcurrentQueue &operator=(const ConcurrentQueue &) = delete;

    ConcurrentQueue(ConcurrentQueue &&) = delete;

    ConcurrentQueue &operator=(ConcurrentQueue &&) = delete;
    /**
     * @brief Creates a new empty concurrent queue.
     */
    ConcurrentQueue() = default;

    ~ConcurrentQueue() = default;

    /**
     * @brief Gets the total size of all objects stored in the queue.
     *
     * @return the size of the queue content
     */
    size_t size() const
    {
        std::unique_lock<std::mutex> lck(mMutex);
        return mSize;
    }

    /**
     * @brief Gets the total number of elements in the queue.
     *
     * @return the number of elements in the queue
     */
    uint64_t nbElements() const
    {
        std::unique_lock<std::mutex> lck(mMutex);
        return mQ.size();
    }

    /**
     * @brief Returns true if the queue has been flushed.
     *
     * Data can remain into the queue, pull it until nullptr comes out.
     * @return true if queue has been flushed, false otherwise
     */
    bool isFlushed() const
    {
        std::unique_lock<std::mutex> lck(mMutex);
        return mFlushed;
    }

    /**
     * @brief Flushes the queue.
     *
     * When the queue has been flushed no more data can be added.
     * A call to get() will not block if the queue is empty AND flushed. In this case get() will return nullptr.
     * All threads waiting in get() are waken up.
     */
    void flush()
    {
        std::unique_lock<std::mutex> lck(mMutex);

        mFlushed = true;
        lck.unlock();
        mCondOut.notify_all();
    }

    /**
     * @brief Empties the queue.
     *
     * This method can only be called on a flushed queue, otherwise it throws.
     */
    void discardAllData()
    {
        CHECK_S(isFlushed()) << "Cannot discard data from a non flushed queue";

        while (auto p = get())
            ;
    }

    /**
     * @brief Adds a new object to the queue and wakes up another thread waiting in get().
     *
     * This method will fail if the queue is flushed.
     * The queue only takes the ownership of the object if the method successed. Otherwise, ownership remains with the caller. 
     * @param pDataRef address of the object to add
     * @return true if pDataRef has been added to the queue, false otherwise
     */
    bool tryPut(std::unique_ptr<T> &&pDataRef)
    {
        std::unique_lock<std::mutex> lck(mMutex);

        if (!pDataRef || mFlushed)
            return false;

        std::unique_ptr<T> pData = std::move(pDataRef);    // take ownership

        mSize += pData->size();
        mQ.push(std::move(pData));
        lck.unlock();
        mCondOut.notify_one();

        return true;
    }

    /**
     * @brief Returns the next available object in the queue.
     *
     * If data is available the method returns immediately.
     * If the queue is empty the method will block until some other thread add data to the queue or flush it.
     * If the queue is empty AND flushed the method will return nullptr immediatly.
     * @return an owning pointer to the next available object in the queue or nullptr
     */
    std::unique_ptr<T> get()
    {
        std::unique_ptr<T> pHead = nullptr;
        std::unique_lock<std::mutex> lck(mMutex);

        while (true)
        {
            if (!mQ.empty())
            {
                pHead = std::move(mQ.front());
                mQ.pop();
                mSize -= pHead->size();

                return pHead;
            }

            if (mFlushed)
                return nullptr;

            mCondOut.wait(lck, [this]() {
                return !mQ.empty() || mFlushed;
            });
        }
    }

    /**
     * @brief Returns the next available object in the queue.
     *
     * If data is available the method returns immediately.
     * If the queue is empty the method will block until the timeout expires or some other thread add data to the queue or flush it.
     * If the timeout expires the method will return nullptr.
     * If the queue is empty AND flushed the method will return nullptr immediatly.
     * @param ms the timeout in milliseconds before returning if no data is available
     * @return an owning pointer to the next available object in the queue or nullptr
     */
    std::unique_ptr<T> get(std::chrono::milliseconds const &ms)
    {
        std::unique_ptr<T> pHead = nullptr;
        std::unique_lock<std::mutex> lck(mMutex);

        if (mQ.empty())
        {
            if (mFlushed)
                return nullptr;

            mCondOut.wait_for(lck, ms, [this]() {
                return !mQ.empty() || mFlushed;
            });
        }

        if (!mQ.empty())
        {
            pHead = std::move(mQ.front());
            mQ.pop();
            mSize -= pHead->size();
        }

        return pHead;
    }
};
