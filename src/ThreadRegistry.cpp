#include "ThreadRegistry.hpp"
#include "ThreadBase.hpp"

ThreadRegistry &ThreadRegistry::get()
{
    static ThreadRegistry instance;

    return instance;
}

bool ThreadRegistry::postMessage(const ThreadMessageBase &msg, const std::string &cat) const
{
    auto range = findThreads(cat);

    if (range.first == range.second)
        return false;

    for (auto it = range.first; it != range.second; ++it)
        it->second->post(msg.allocateCopy());

    return true;
}

void ThreadRegistry::joinAll() const
{
    LOG_SCOPE_F(INFO, "Joining all non joined threads...");
    for (auto &pair : mRegister)
    {
        auto &pThread = pair.second;
        if (pThread->waitThread())
            LOG_S(INFO) << "Thread '" << pThread->getThreadName() << "' joined";
    }
}

std::pair<ThreadRegistry::MmapType::const_iterator, ThreadRegistry::MmapType::const_iterator> ThreadRegistry::findThreads(const std::string &cat) const
{
    std::pair<MmapType::const_iterator, MmapType::const_iterator> range;

    if (cat == "all")
    {
        range.first  = mRegister.cbegin();
        range.second = mRegister.cend();
    }
    else
        range = mRegister.equal_range(cat);

    return range;
}
