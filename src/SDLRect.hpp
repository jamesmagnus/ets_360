/**
 * @file SDLRect.hpp
 * @author Jean-Loup Beaussart
 * @class SDL::Rect
 * @brief OOP wrapper for SDL_Rect structure.
 *
 */

#pragma once

#include <SDL_rect.h>
#include <memory>

namespace SDL {
class Rect {
private:
    std::unique_ptr<SDL_Rect> mpRect;

public:
    Rect(const Rect &origin);
    Rect &operator      =(const Rect &origin);
    Rect(Rect &&origin) = default;
    Rect &operator=(Rect &&origin) = default;
    Rect()                         = default;
    ~Rect()                        = default;

    Rect(int x, int y, int w, int h);

    const std::unique_ptr<SDL_Rect> &SDLPtr() const;
};
}    // namespace SDL
