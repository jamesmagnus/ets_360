#pragma once

#include "LoguruLogger.hpp"
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <type_traits>

class ThreadBase;
class ThreadMessageBase;

class ThreadRegistry {
    using MmapType = std::multimap<std::string, std::unique_ptr<ThreadBase>>;

private:
    mutable std::mutex mMutex;
    MmapType mRegister;

    ThreadRegistry() = default;

    std::pair<MmapType::const_iterator, MmapType::const_iterator> findThreads(const std::string &cat) const;

public:
    ThreadRegistry &operator=(const ThreadRegistry &) = delete;
    ThreadRegistry(const ThreadRegistry &)            = delete;
    ThreadRegistry(ThreadRegistry &&)                 = delete;
    ThreadRegistry &operator=(ThreadRegistry &&) = delete;

    static ThreadRegistry &get();

    ~ThreadRegistry() = default;

    template <class T, class... Args>
    T &newThread(const std::string &category, Args &&... args)
    {
        static_assert(std::is_base_of<ThreadBase, T>(), "Only class derived from ThreadBase can be constructed by the thread registry");
        auto pThread = std::make_unique<T>(std::forward<Args>(args)...);
        std::unique_lock<std::mutex> lck(mMutex);
        auto it = mRegister.insert(std::make_pair(category, std::move(pThread)));

        return dynamic_cast<T &>(*it->second);
    }

    bool postMessage(const ThreadMessageBase &msg, const std::string &cat = "all") const;

    void joinAll() const;
};
