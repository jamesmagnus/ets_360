/**
 * @file Segment.hpp
 * @author Jean-Loup Beaussart
 * @class Segment
 * @brief This class represents a DASH segment.
 *
 * Basically it's just a DownloadObserver with a dash::mpd::ISegment and a buffer for received data.
 * It allows reading of bloc of data with optional offset.
 * If all the required data is not downloaded read() will bloc.
 */

#pragma once

#include "DownloadObserver.hpp"
#include <memory>
#include <vector>

namespace dash {
namespace mpd {
    class ISegment;
}    // namespace mpd
}    // namespace dash

class Segment {
private:
    std::unique_ptr<DownloadObserver> mpDownloadObsv;
    dash::mpd::ISegment &mSeg;
    std::vector<unsigned char> mExtractedData;
    uint64_t mCursor;

public:
    explicit Segment(dash::mpd::ISegment &initSeg);

    std::vector<unsigned char>::iterator read(uint64_t len, uint64_t origin);

    std::vector<unsigned char>::iterator read(uint64_t len);

    uint64_t totalSize() const;

    uint64_t downloadSpeed() const;

    bool startDownload();

    void stopDownload();
};
