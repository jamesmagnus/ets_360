#include "SDLSystem.hpp"
#include "LoguruLogger.hpp"
#include "Overlay.hpp"
#include "SDLRect.hpp"
#include "SDLTexture.hpp"
#include "Sample.hpp"
#include <algorithm>
#include <iostream>
#include <sstream>

namespace SDL {
System::System(Uint32 flags, std::string const &winName, int w, int h)
    : mpWin(nullptr, SDL_DestroyWindow),
      mpRenderer(nullptr, SDL_DestroyRenderer),
      mLogicalWidth(w),
      mLogicalHeight(h),
      mAudioDevice(0)
{
    CHECK_S(SDL_Init(flags) == 0) << printSDLFailure("Could not initialize SDL");

    findWindowSize(&w, &h);
    mpWin = std::unique_ptr<SDL_Window, void (*)(SDL_Window *)>(SDL_CreateWindow(winName.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, w, h, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE), SDL_DestroyWindow);

    if (!mpWin)
    {
        SDL_Quit();
        ABORT_S() << printSDLFailure("Could not set video mode");
    }

    mpRenderer = std::unique_ptr<SDL_Renderer, void (*)(SDL_Renderer *)>(SDL_CreateRenderer(mpWin.get(), -1, SDL_RENDERER_ACCELERATED), SDL_DestroyRenderer);

    if (!mpRenderer)
    {
        SDL_DestroyWindow(mpWin.get());
        SDL_Quit();
        ABORT_S() << printSDLFailure("Could not create rendering context");
    }

    SDL_RenderSetLogicalSize(mpRenderer.get(), mLogicalWidth, mLogicalHeight);
    SDL_SetRenderDrawColor(mpRenderer.get(), 0, 0, 0, 255);
    SDL_RenderClear(mpRenderer.get());
    SDL_RenderPresent(mpRenderer.get());

    LOG_S(INFO) << "SDL: successfully initialized";
}

System::~System()
{
    SDL_Quit();
    LOG_S(INFO) << "SDL: shut down";
}

void System::setOverlay(std::unique_ptr<::Overlay> pOverlay)
{
    mpOverlay = std::move(pOverlay);
}

SDL_AudioDeviceID System::setupAudio(int sampleRate)
{
    SDL_AudioSpec wantedSpec, spec;

    wantedSpec.freq     = sampleRate;
    wantedSpec.format   = AUDIO_S16SYS;
    wantedSpec.channels = 2;
    wantedSpec.silence  = 0;
    wantedSpec.samples  = 1024;
    wantedSpec.callback = nullptr;
    wantedSpec.userdata = nullptr;

    mAudioDevice = SDL_OpenAudioDevice(nullptr, false, &wantedSpec, &spec, 0);
    CHECK_S(mAudioDevice > 0) << SDL_GetError();

    if (spec.freq != wantedSpec.freq)
        LOG_S(WARNING) << "Cannot set output audio frequency to " << wantedSpec.freq;

    SDL_PauseAudioDevice(mAudioDevice, false);
    return mAudioDevice;
}

void System::feedAudio(std::unique_ptr<Sample> pSample) const
{
    CHECK_S(SDL_QueueAudio(mAudioDevice, pSample->raw()[0], static_cast<uint32_t>(pSample->size())) == 0) << SDL_GetError();
}

std::string System::printSDLFailure(std::string const &msg)
{
    std::stringstream errMsg;

    errMsg << "SDL: " << msg << " - " << SDL_GetError();
    std::cerr << errMsg.str() << std::endl;
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Player - Error", errMsg.str().c_str(), nullptr);

    return errMsg.str();
}

Texture System::newTexture(Uint32 pixFormat, int access, int w, int h) const
{
    if (w == 0 && h == 0)
    {
        w = mLogicalWidth;
        h = mLogicalHeight;
    }

    return Texture(mpRenderer.get(), pixFormat, access, w, h);
}

Texture System::newTexture(std::unique_ptr<SDL_Surface, void (*)(SDL_Surface *)> pSurface) const
{
    return Texture(mpRenderer.get(), std::move(pSurface));
}

void System::renderClear() const
{
    SDL_RenderClear(mpRenderer.get());
}

void System::renderPresent() const
{
    if (mpOverlay)
        mpOverlay->updateOverlay();

    SDL_RenderPresent(mpRenderer.get());
}

void System::renderCopy(Texture &tex, Rect &rectSrc, Rect &rectDst) const
{
    SDL_RenderCopy(mpRenderer.get(), tex.SDLPtr().get(), rectSrc.SDLPtr().get(), rectDst.SDLPtr().get());
}

void System::renderCopy(Texture &tex) const
{
    Rect r;
    renderCopy(tex, r, r);
}

int System::renderWidth() const
{
    return mLogicalWidth;
}

int System::renderHeight() const
{
    return mLogicalHeight;
}

void System::findWindowSize(int *pVideoW, int *pVideoH)
{
    int nDisplays = SDL_GetNumVideoDisplays();
    LOG_S(INFO) << "Found " << nDisplays << " screens";

    SDL_DisplayMode mode;
    SDL_GetCurrentDisplayMode(0, &mode);    // TODO let user choose screen
    int screenW = mode.w;
    int screenH = mode.h;
    LOG_S(INFO) << "Screen resolution " << screenW << "x" << screenH;
    *pVideoW = std::min(*pVideoW, screenW);
    *pVideoH = std::min(*pVideoH, screenH);
}
}    // namespace SDL
