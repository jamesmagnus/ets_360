/**
 * @file SDLOverlay.hpp
 * @author Jean-Loup Beaussart
 * @class SDL::Overlay
 * @brief Makes the SDL draw calls for overlays and renders text using SDL_ttf
 *
 * This is the interface between Overlay derived classes and SDL functions.
 * It just provides a way of writing some text at some position of the renderer.
 */

#pragma once

#include <SDL.h>
#include <SDL_ttf.h>
#include <memory>
#include <string>

namespace SDL {
class System;
class Rect;

class Overlay {
private:
    std::unique_ptr<System> &mpSys;
    std::unique_ptr<TTF_Font, void (*)(TTF_Font *)> mpFont;
    const SDL_Color mRenderColor;

public:
    Overlay(const Overlay &) = delete;
    Overlay &operator=(const Overlay &) = delete;
    Overlay(Overlay &&)                 = delete;
    Overlay &operator=(Overlay &&) = delete;
    Overlay(std::unique_ptr<System> &pSDLSys, const std::string &font, int size = 20);

    ~Overlay();

    void write(const std::string &msg, Rect &position);
};
}    // namespace SDL
