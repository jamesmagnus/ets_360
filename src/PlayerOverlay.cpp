#include "PlayerOverlay.hpp"
#include "Clock.hpp"
#include "ConcurrentQueue.hpp"
#include <algorithm>
#include <numeric>

PlayerOverlay::PlayerOverlay(std::unique_ptr<SDL::System> &pSDLSys, std::shared_ptr<const Clock> pcClock, std::unique_ptr<ConcurrentQueue<Packet>> &pInputVideoQ, std::unique_ptr<ConcurrentQueue<Packet>> &pInputAudioQ, std::vector<std::unique_ptr<ConcurrentQueue<YUV>>> &pVideoQs, std::unique_ptr<ConcurrentQueue<Sample>> &pSampleQ)
    : Overlay(pSDLSys),
      mpcClock(std::move(pcClock)),
      mpInputVideoQ(pInputVideoQ),
      mpInputAudioQ(pInputAudioQ),
      mpVideoQs(pVideoQs),
      mpSampleQ(pSampleQ)
{
    dataLabels().emplace_back("Time");
    dataLabels().emplace_back("Video Q");
    dataLabels().emplace_back("Audio Q");
    dataLabels().emplace_back("Packet video Q");
    dataLabels().emplace_back("Packet audio Q");
}

void PlayerOverlay::setOverlayData()
{
    std::for_each(dataLabels().cbegin(), dataLabels().cend(), [this](auto label) {
        dataValues()[label].str("");
    });

    dataValues()["Time"] << static_cast<size_t>((mpcClock->timeInMs() / 1000).count());
    dataValues()["Audio Q"] << mpSampleQ->size();
    size_t sum = std::accumulate(mpVideoQs.begin(), mpVideoQs.end(), 0UL, [](auto acc, auto &q) {
        return acc += q->size();
    });
    dataValues()["Video Q"] << sum;
    dataValues()["Packet audio Q"] << mpInputAudioQ->size();
    dataValues()["Packet video Q"] << mpInputVideoQ->size();
}
