#include "Overlay.hpp"
#include "SDLRect.hpp"
#include <algorithm>
#include <sstream>

Overlay::Overlay(std::unique_ptr<SDL::System> &pSDLSys)
    : mOverlaySys(pSDLSys, "FreeSans.ttf")
{
}

void Overlay::updateOverlay()
{
    SDL::Rect pos(10, 10, 0, 0);

    setOverlayData();

    std::for_each(mOverlayDataLabels.cbegin(), mOverlayDataLabels.cend(), [this, &pos](auto label) {
        std::stringstream ss;
        ss << label << ": " << mOverlayDataValues[label].str();
        mOverlaySys.write(ss.str(), pos);
        pos.SDLPtr()->y += 25;
    });
}

std::map<std::string, std::stringstream> &Overlay::dataValues()
{
    return mOverlayDataValues;
}

std::vector<std::string> &Overlay::dataLabels()
{
    return mOverlayDataLabels;
}
