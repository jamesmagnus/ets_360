#include "SDLRect.hpp"

namespace SDL {
Rect::Rect(const Rect &origin)
{
    *this = origin;
}

Rect &Rect::operator=(const Rect &origin)
{
    if (this != &origin)
    {
        if (!origin.SDLPtr())
            mpRect = nullptr;
        else
        {
            if (!SDLPtr())
                mpRect = std::make_unique<SDL_Rect>();

            SDLPtr()->x = origin.SDLPtr()->x;
            SDLPtr()->y = origin.SDLPtr()->y;
            SDLPtr()->w = origin.SDLPtr()->w;
            SDLPtr()->h = origin.SDLPtr()->h;
        }
    }

    return *this;
}

Rect::Rect(int x, int y, int w, int h)
    : mpRect(std::make_unique<SDL_Rect>())
{
    SDLPtr()->x = x;
    SDLPtr()->y = y;
    SDLPtr()->w = w;
    SDLPtr()->h = h;
}

const std::unique_ptr<SDL_Rect> &Rect::SDLPtr() const
{
    return mpRect;
}
}    // namespace SDL
