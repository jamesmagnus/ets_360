/**
 * @file SDLTexture.hpp
 * @author Jean-Loup Beaussart
 * @class SDL::Texture
 * @brief OOP wrapper for SDL_Texture
 *
 * It provides ease to update the texture data with different formats.
 */

#pragma once

#include <SDL.h>
#include <memory>

namespace SDL {
class Rect;

using unique_ptr_SDLTexture = std::unique_ptr<SDL_Texture, void (*)(SDL_Texture *)>;

class Texture {
private:
    unique_ptr_SDLTexture mpTex;

public:
    Texture(SDL_Renderer *pRen, Uint32 pixFormat, int access, int w, int h);
    Texture(SDL_Renderer *pRen, std::unique_ptr<SDL_Surface, void (*)(SDL_Surface *)> pSurface);

    void update(Rect &rect, const void *pixels, int pitch) const;

    void update(const void *pixels, int pitch) const;

    void update(Rect &rect, const Uint8 *Y, int YPitch, const Uint8 *U, int UPitch, const Uint8 *V, int VPitch) const;

    void update(const Uint8 *Y, int YPitch, const Uint8 *U, int UPitch, const Uint8 *V, int VPitch) const;

    const unique_ptr_SDLTexture &SDLPtr() const;
};
}    // namespace SDL
