/**
 * @file DownloadObserver.hpp
 * @author Jean-Loup Beaussart
 * @class DownloadObserver
 * @brief An download state observer for DASH segments.
 *
 * Implements the dash::network::IDownloadObserver interface from libdash.
 * Counts the total number of downloaded bytes and provides download speed average measure.
 */

#pragma once

#include <chrono>
#include <libdash.h>

class DownloadObserver : public dash::network::IDownloadObserver {
    using Clock = std::chrono::steady_clock;

private:
    uint64_t mNbBytesLastCall{0};
    Clock::time_point mBeginning;
    Clock::time_point mEnd;
    bool mCompleted{false};

public:
    DownloadObserver &operator=(const DownloadObserver &) = default;
    DownloadObserver &operator=(DownloadObserver &&) = default;
    DownloadObserver(DownloadObserver &&)            = default;
    DownloadObserver(const DownloadObserver &)       = default;
    DownloadObserver()                               = default;
    ~DownloadObserver() override                     = default;

    void OnDownloadRateChanged(uint64_t bytesDownloaded) override;

    void OnDownloadStateChanged(dash::network::DownloadState state) override;

    uint64_t averageDLSpeedBps() const;

    bool isComplete() const;

    uint64_t totalSize() const;
};
