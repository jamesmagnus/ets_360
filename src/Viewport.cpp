#define _USE_MATH_DEFINES
#include "Viewport.hpp"
#include "LoguruLogger.hpp"
#include <cmath>

const float cPi = static_cast<float>(M_PI);

float Viewport::radToDeg(float rad)
{
    return rad * 180.0f / cPi;
}

float Viewport::degToRad(float deg)
{
    return deg * cPi / 180.0f;
}

Viewport::Viewport(int viewportW, int viewportH, float horizontalAperture, float verticalAperture)
    : mPitch(0),
      mYaw(0),
      mHorizFOVRadian(degToRad(horizontalAperture)),
      mVertFOVRadian(degToRad(verticalAperture)),
      mViewportW(viewportW),
      mViewportH(viewportH),
      mViewportIn3D(viewportTo3D())
{
}

float Viewport::yaw() const
{
    return mYaw;
}

void Viewport::yaw(float yaw)
{
    CHECK_S(yaw <= cPi / 2.0f && yaw >= cPi / 2.0f);
    mYaw = yaw;
}

float Viewport::pitch() const
{
    return mPitch;
}

void Viewport::pitch(float pitch)
{
    CHECK_S(pitch <= cPi && pitch >= -cPi);
    mPitch = pitch;
}

std::array<Eigen::Vector2i, 4> Viewport::viewportBound(int w, int h) const
{
    Eigen::Vector2i topLeft, topRight, bottomLeft, bottomRight;
    auto rotated = rotate3D();

    // TODO use interpolation instead of cast<int> ?
    topLeft     = sphere3DToERP(rotated.col(0), w, h).cast<int>();
    topRight    = sphere3DToERP(rotated.col(w - 1), w, h).cast<int>();
    bottomLeft  = sphere3DToERP(rotated.col((h - 1) * w), w, h).cast<int>();
    bottomRight = sphere3DToERP(rotated.col(w * h - 1), w, h).cast<int>();

    return {topLeft, topRight, bottomLeft, bottomRight};
}

Eigen::MatrixXf Viewport::viewportTo3D() const
{
    Eigen::MatrixXf sphereCoord(3, mViewportW * mViewportH);

    float tanHalfHorizFOV = std::tan(mHorizFOVRadian / 2.0f);
    float tanHalfVertFOV  = std::tan(mVertFOVRadian / 2.0f);

    for (int i = 0; i < mViewportW; ++i)
        for (int j = 0; j < mViewportH; ++j)
        {
            float x = (i + 0.5f) * 2.0f * (tanHalfHorizFOV / mViewportW) - tanHalfHorizFOV;
            float y = tanHalfVertFOV - (j + 0.5f) * 2.0f * (tanHalfVertFOV / mViewportH);
            float z = 1.0f;

            float sphereRadius = std::sqrt(x * x + y * y + z * z);

            Eigen::Vector3f vecOnSphere(x / sphereRadius, y / sphereRadius, z / sphereRadius);
            sphereCoord.col(i + j * mViewportW) = vecOnSphere;
        }

    return sphereCoord;
}

Eigen::MatrixXf Viewport::rotate3D() const
{
    Eigen::Matrix3f RZ, RY;
    float cosYaw        = std::cos(mYaw);
    float sinYaw        = std::sin(mYaw);
    float cosMinusPitch = std::cos(-mPitch);
    float sinMinusPitch = std::sin(-mPitch);

    RY << cosYaw, 0.0f, sinYaw,
        0.0f, 1.0f, 0.0f,
        -sinYaw, 0.0f, cosYaw;

    RZ << 1.0f, 0.0f, 0.0f,
        0.0f, cosMinusPitch, -sinMinusPitch,
        0.0f, sinMinusPitch, cosMinusPitch;

    return RY * RZ * mViewportIn3D;
}

Eigen::Vector2f Viewport::sphere3DToERP(const Eigen::Vector3f &sphereCoord, float erpW, float erpH) const
{
    float x = sphereCoord(0);
    float y = sphereCoord(1);
    float z = sphereCoord(2);

    float phi   = std::atan2(-z, x);
    y           = y > 1.0f ? 1.0f : y;
    float theta = std::asin(y);

    float m = erpW * ((phi / (2.0f * cPi)) + 0.5f) - 0.5f;
    float n = erpH * (0.5f - (theta / cPi)) - 0.5f;

    Eigen::Vector2f vecERP(m, n);

    return vecERP;
}
