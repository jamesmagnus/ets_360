#include "SDLTexture.hpp"
#include "LoguruLogger.hpp"
#include "SDLRect.hpp"
#include "SDLSystem.hpp"

namespace SDL {
Texture::Texture(SDL_Renderer *pRen, Uint32 pixFormat, int access, int w, int h)
    : mpTex(SDL_CreateTexture(pRen, pixFormat, access, w, h), SDL_DestroyTexture)
{
    CHECK_S(mpTex != nullptr) << System::printSDLFailure("Cannot create new texture");
}

Texture::Texture(SDL_Renderer *pRen, std::unique_ptr<SDL_Surface, void (*)(SDL_Surface *)> pSurface)
    : mpTex(SDL_CreateTextureFromSurface(pRen, pSurface.get()), SDL_DestroyTexture)
{
    CHECK_S(mpTex != nullptr) << System::printSDLFailure("Cannot create new texture from surface");
}

void Texture::update(Rect &rect, const void *pixels, int pitch) const
{
    CHECK_S(SDL_UpdateTexture(SDLPtr().get(), rect.SDLPtr().get(), pixels, pitch) == 0) << System::printSDLFailure("Cannot update texture");
}

void Texture::update(const void *pixels, int pitch) const
{
    Rect r;
    update(r, pixels, pitch);
}

void Texture::update(Rect &rect, const Uint8 *Y, int YPitch, const Uint8 *U, int UPitch, const Uint8 *V, int VPitch) const
{
    CHECK_S(SDL_UpdateYUVTexture(SDLPtr().get(), rect.SDLPtr().get(), Y, YPitch, U, UPitch, V, VPitch) == 0) << System::printSDLFailure("Cannot update YUV texture");
}

void Texture::update(const Uint8 *Y, int YPitch, const Uint8 *U, int UPitch, const Uint8 *V, int VPitch) const
{
    Rect r;
    update(r, Y, YPitch, U, UPitch, V, VPitch);
}

const unique_ptr_SDLTexture &Texture::SDLPtr() const
{
    return mpTex;
}
}    // namespace SDL
