#include "MediaReader.hpp"
#include "AudioDecoder.hpp"
#include "Clock.hpp"
#include "ConcurrentQueue.hpp"
#include "LoguruLogger.hpp"
#include "VideoDecoder.hpp"
#include <iostream>

using namespace std::chrono_literals;

const int MediaReader::scErrBuffSize                            = 512;
const std::chrono::milliseconds MediaReader::scPacketBufferTime = 3s;

static void deleteAVCodecContext(AVCodecContext *p)
{
    avcodec_free_context(&p);
}

static void deleteAVFormatContext(AVFormatContext *p)
{
    avformat_close_input(&p);
}

MediaReader::MediaReader(std::string const &file, std::unique_ptr<ConcurrentQueue<Packet>> &pVideoQ, std::unique_ptr<ConcurrentQueue<Packet>> &pAudioQ, const VideoData &videoData)
    : ThreadBase("media reader"),
      mpFormatCtx(nullptr, deleteAVFormatContext),
      mpVideoCodecCtx(nullptr, deleteAVCodecContext),
      mpAudioCodecCtx(nullptr, deleteAVCodecContext),
      mpVideoCodec(nullptr),
      mpAudioCodec(nullptr),
      mpAudioDecThread(nullptr),
      mpVideoDecThread(nullptr),
      mErrCde(0),
      mVideoStream(-1),
      mAudioStream(-1),
      mStop(false),
      mpVideoQ(pVideoQ),
      mpAudioQ(pAudioQ),
      mpcClock(nullptr)
{
    CHECK_S(mpVideoQ != nullptr);
    CHECK_S(mpAudioQ != nullptr);

    AVFormatContext *pFmtCtx = nullptr;

    if ((mErrCde = avformat_open_input(&pFmtCtx, file.c_str(), nullptr, nullptr)) < 0)
        throwAVLibError();
    mpFormatCtx.reset(pFmtCtx);

    if ((mErrCde = avformat_find_stream_info(mpFormatCtx.get(), nullptr)) < 0)
        throwAVLibError();

    // dump information about file format to stderr
    av_dump_format(mpFormatCtx.get(), 0, file.c_str(), 0);

    for (int i = 0; i < static_cast<int>(mpFormatCtx->nb_streams); ++i)
    {
        if (mVideoStream < 0 && mpFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
            mVideoStream = i;

        if (mAudioStream < 0 && mpFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
            mAudioStream = i;
    }

    if (mVideoStream == -1)
        throwError("No video stream found in " + file);

    if (mAudioStream == -1)
        std::cerr << "No audio stream found in " << file << std::endl;

    if (videoData.useHwAccel)
    {
        try
        {
            setupCodecs();
            streamComponentOpen();
        }
        catch (std::exception &)
        {
            std::cerr << "Codec not supported by your graphic card, falling back to sofware codec" << std::endl;
            setupCodecs(false);
            streamComponentOpen();
        }
    }
    else
    {
        setupCodecs(false);
        streamComponentOpen();
    }
}

void MediaReader::setupCodecs(bool hwAccel)
{
    AVCodecParameters *pVideoCodecParam;
    AVCodecParameters *pAudioCodecParam = nullptr;

    pVideoCodecParam = mpFormatCtx->streams[mVideoStream]->codecpar;
    mpVideoCodec     = findCodecAuto(pVideoCodecParam);
    if (hwAccel)
    {
        std::string name(mpVideoCodec->name);
        name += "_cuvid";
        auto p = findCodecByName(name);
        if (p != nullptr)
        {
            mpVideoCodec = p;
            std::cout << "Hardware accelerated codec available" << std::endl;
        }
    }
    std::cout << "Using video codec: " << mpVideoCodec->name << std::endl;

    if (hasAudio())
    {
        pAudioCodecParam = mpFormatCtx->streams[mAudioStream]->codecpar;
        mpAudioCodec     = findCodecAuto(pAudioCodecParam);
        std::cout << "Using audio codec: " << mpAudioCodec->name << std::endl;
    }

    if (mpVideoCodec == nullptr || (hasAudio() && mpAudioCodec == nullptr))
        throwError("Unsupported codec!");

    if (hasAudio())
    {
        mpAudioCodecCtx.reset(avcodec_alloc_context3(mpAudioCodec), deleteAVCodecContext);
        if ((mErrCde = avcodec_parameters_to_context(mpAudioCodecCtx.get(), pAudioCodecParam)) < 0)
            throwAVLibError();
    }

    mpVideoCodecCtx.reset(avcodec_alloc_context3(mpVideoCodec), deleteAVCodecContext);
    if ((mErrCde = avcodec_parameters_to_context(mpVideoCodecCtx.get(), pVideoCodecParam)) < 0)
        throwAVLibError();
}

AVCodec *MediaReader::findCodecAuto(AVCodecParameters *codecParams) const
{
    return avcodec_find_decoder(codecParams->codec_id);
}

AVCodec *MediaReader::findCodecByName(const std::string &name) const
{
    return avcodec_find_decoder_by_name(name.c_str());
}

void MediaReader::streamComponentOpen()
{
    if ((mErrCde = avcodec_open2(mpVideoCodecCtx.get(), mpVideoCodec, nullptr)) < 0)
        throwAVLibError();

    if (hasAudio() && (mErrCde = avcodec_open2(mpAudioCodecCtx.get(), mpAudioCodec, nullptr)) < 0)
        throwAVLibError();
}

bool MediaReader::threadBegin()
{
    mpVideoDecThread->startThread();
    if (hasAudio())
        mpAudioDecThread->startThread();

    return true;
}

bool MediaReader::threadWork()
{
    return !mStop && processNextPackets();
}

void MediaReader::threadHandleMsg(const ThreadMessageBase &msg)
{
    switch (msg.getType())
    {
    case EMsgType::USER_QUIT:
        mStop = true;
        break;
    case EMsgType::FOV_CHANGE:
        break;
    }
}

void MediaReader::threadEnd()
{
    mpVideoQ->flush();
    mpAudioQ->flush();
    mpVideoDecThread->waitThread();

    if (hasAudio())
        mpAudioDecThread->waitThread();
}

// TODO check : should not be called after thread start
int MediaReader::videoWidth() const
{
    return mpVideoCodecCtx->width;
}

int MediaReader::videoHeight() const
{
    return mpVideoCodecCtx->height;
}

AVRational MediaReader::audioTimebase() const
{
    if (hasAudio())
        return mpFormatCtx->streams[mAudioStream]->time_base;
    return AVRational{1, 1};
}

AVRational MediaReader::videoTimebase() const
{
    return mpFormatCtx->streams[mVideoStream]->time_base;
}

std::shared_ptr<AVCodecContext> MediaReader::audioCtx() const
{
    return mpAudioCodecCtx;
}

std::shared_ptr<AVCodecContext> MediaReader::videoCtx() const
{
    return mpVideoCodecCtx;
}

//

bool MediaReader::processNextPackets()
{
    int ret = 0;

    auto pPacket = std::make_unique<Packet>();
    ret          = av_read_frame(mpFormatCtx.get(), pPacket->avPacket().get());

    if (ret < 0)
        return false;

    auto dts = pPacket->dts();

    if (pPacket->avPacket().get()->stream_index == mVideoStream)
    {
        auto dtsMs = mpcClock->videoPtsToMs(dts);
        sleepUntilDecodeTime(dtsMs);
        mpVideoQ->tryPut(std::move(pPacket));
    }
    else if (pPacket->avPacket().get()->stream_index == mAudioStream)
    {
        auto dtsMs = mpcClock->audioPtsToMs(dts);
        sleepUntilDecodeTime(dtsMs);
        mpAudioQ->tryPut(std::move(pPacket));
    }

    return true;
}

void MediaReader::attachAudioDecoder(AudioDecoder &audioDec)
{
    mpAudioDecThread = &audioDec;
}

void MediaReader::attachVideoDecoder(VideoDecoder &videoDec)
{
    mpVideoDecThread = &videoDec;
}

void MediaReader::attachPlaybackClock(std::shared_ptr<const Clock> pcClk)
{
    mpcClock = std::move(pcClk);
}

void MediaReader::throwAVLibError() const
{
    char err[scErrBuffSize];

    av_strerror(mErrCde, err, scErrBuffSize);
    std::cerr << err << std::endl;
    throw std::runtime_error(err);
}

void MediaReader::throwError(std::string const &msg) const
{
    std::cerr << msg << std::endl;
    throw std::runtime_error(msg);
}

bool MediaReader::hasAudio() const
{
    return mAudioStream != -1;
}

void MediaReader::sleepUntilDecodeTime(std::chrono::milliseconds dts)
{
    auto timeBeforeDecoding = dts - mpcClock->timeInMs();

    if (timeBeforeDecoding > scPacketBufferTime)
        std::this_thread::sleep_for(timeBeforeDecoding - scPacketBufferTime);
}
