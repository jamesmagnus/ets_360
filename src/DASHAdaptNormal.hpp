/**
 * @file DASHAdaptNormal.hpp
 * @author Jean-Loup Beaussart
 * @class DASHAdaptNormal
 * @brief A basic implementation of DASH adaptation logic for regular DASH video
 *
 * This class implements adaptation logic for regular (i.e. non 360) video.
 * Basically it just downloads the next segment with the highest available quality that fit in the client bandwidth.
 */

#pragma once

#include "DASHAdapt.hpp"

class DASHAdaptNormal : public DASHAdapt {
private:
    bool logic() override;
    void initLogic() override;

public:
    DASHAdaptNormal(const DASHAdaptNormal &) = delete;
    DASHAdaptNormal &operator=(const DASHAdaptNormal &) = delete;
    DASHAdaptNormal(DASHAdaptNormal &&)                 = delete;
    DASHAdaptNormal &operator=(DASHAdaptNormal &&) = delete;

    explicit DASHAdaptNormal(std::vector<std::reference_wrapper<NamedPipe<unsigned char>>> &pipes);

    ~DASHAdaptNormal() override = default;
};
