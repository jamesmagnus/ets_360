#include "Clock.hpp"
#include "LoguruLogger.hpp"

Clock::Clock(AVRational tbVideo, AVRational tbAudio)
    : mcOneMs(1, 1000),
      mVideoTimebaseMs(convertTimebaseToMs(tbVideo)),
      mAudioTimebaseMs(convertTimebaseToMs(tbAudio)),
      mOrigin(Clk::now())
{
}

Clock::Clock()
    : mcOneMs(1, 1000),
      mVideoTimebaseMs(0),
      mAudioTimebaseMs(0),
      mOrigin(Clk::now())
{
}

std::chrono::milliseconds Clock::timeInMs() const
{
    std::unique_lock<std::mutex> lck(mMutex);
    return std::chrono::duration_cast<std::chrono::milliseconds>(Clk::now() - mOrigin);
}

std::chrono::milliseconds Clock::videoPtsToMs(int64_t pts) const
{
    return std::chrono::milliseconds(boost::rational_cast<int64_t>(pts * mVideoTimebaseMs));
}

std::chrono::milliseconds Clock::audioPtsToMs(int64_t pts) const
{
    return std::chrono::milliseconds(boost::rational_cast<int64_t>(pts * mAudioTimebaseMs));
}

std::chrono::milliseconds Clock::reset()
{
    auto clk = timeInMs();
    std::unique_lock<std::mutex> lck(mMutex);
    mOrigin = Clk::now();

    return clk;
}

void Clock::setVideoTimebase(AVRational tb)
{
    LOG_IF_S(WARNING, mVideoTimebaseMs != 0) << "Changing video timebase which is already set. Strange behaviour may occurs.";
    mVideoTimebaseMs = convertTimebaseToMs(tb);
}

void Clock::setAudioTimebase(AVRational tb)
{
    LOG_IF_S(WARNING, mAudioTimebaseMs != 0) << "Changing audio timebase which is already set. Strange behaviour may occurs.";
    mAudioTimebaseMs = convertTimebaseToMs(tb);
}

boost::rational<int64_t> Clock::convertTimebaseToMs(AVRational tb)
{
    return boost::rational<int64_t>(tb.num * mcOneMs.denominator(), tb.den * mcOneMs.numerator());
}
