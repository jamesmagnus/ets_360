#pragma once

#include <Eigen/Core>
#include <array>

class Viewport {
private:
    float mPitch;
    float mYaw;
    float mHorizFOVRadian;
    float mVertFOVRadian;
    int mViewportW;
    int mViewportH;
    Eigen::MatrixXf mViewportIn3D;

    Eigen::MatrixXf viewportTo3D() const;
    Eigen::MatrixXf rotate3D() const;
    Eigen::Vector2f sphere3DToERP(const Eigen::Vector3f &sphereCoord, float erpW, float erpH) const;

public:
    static float degToRad(float deg);
    static float radToDeg(float rad);

    Viewport(int viewportW, int viewportH, float horizontalAperture, float verticalAperture);

    float yaw() const;

    void yaw(float yaw);

    float pitch() const;

    void pitch(float pitch);

    std::array<Eigen::Vector2i, 4> viewportBound(int w, int h) const;
};
