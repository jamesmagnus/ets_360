/**
 * @file VideoDecoder.hpp
 * @author Jean-Loup Beaussart
 * @class VideoDecoder
 * @brief A Decoder specialized in video frame decoding.
 * 
 * It decodes packets using ffmpeg library and puts the raw frame (YUV) into an output queue.
 */

#pragma once

#include "Decoder.hpp"
#include "YUV.hpp"

struct SwsContext;

template <class T>
class ConcurrentQueue;

class VideoDecoder : public Decoder {
private:
    static const AVPixelFormat scOutVideoPixFormat;
    static const int scAlign;
    std::unique_ptr<SwsContext, void (*)(SwsContext *)> mpSwsCtx;
    std::unique_ptr<ConcurrentQueue<YUV>> &mpQYUV;

    bool consumeFrame() override;

public:
    VideoDecoder(const VideoDecoder &) = delete;
    VideoDecoder &operator=(const VideoDecoder &) = delete;
    VideoDecoder(VideoDecoder &&)                 = delete;
    VideoDecoder &operator=(VideoDecoder &&) = delete;
    VideoDecoder(std::shared_ptr<AVCodecContext> pCtx, std::unique_ptr<ConcurrentQueue<Packet>> &inputQ, std::unique_ptr<ConcurrentQueue<YUV>> &pQOut);

    ~VideoDecoder() override = default;
};
