#include "Decoder.hpp"
#include "ConcurrentQueue.hpp"

extern "C" {
#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable : 4244)
#endif

#include <libavcodec/avcodec.h>

#ifdef _WIN32
#pragma warning(pop)
#endif
}

Decoder::Decoder(std::shared_ptr<AVCodecContext> pCtx, std::unique_ptr<ConcurrentQueue<Packet>> &inputQ, const std::string &threadName)
    : ThreadBase(threadName),
      mpCodecCtx(std::move(pCtx)),
      mpLastFrame(av_frame_alloc(), [](AVFrame *p) { av_frame_free(&p); }),
      mInputQ(inputQ)
{
}

bool Decoder::threadBegin()
{
    return true;
}

bool Decoder::threadWork()
{
    bool ret          = true;
    int decoderStatus = 0;
    auto pPacket      = mInputQ->get();

    if (pPacket)
    {
        avcodec_send_packet(codecCtx().get(), pPacket->avPacket().get());
        decoderStatus = avcodec_receive_frame(codecCtx().get(), currentFrame().get());
        while (decoderStatus == 0)
        {
            if (!consumeFrame())
            {
                av_frame_unref(currentFrame().get());
                break;
            }

            av_frame_unref(currentFrame().get());
            decoderStatus = avcodec_receive_frame(codecCtx().get(), currentFrame().get());
        }
    }

    if (decoderStatus == AVERROR_EOF || !pPacket)
    {
        mpLastFrame = nullptr;    // informs derived implementation that it's over
        consumeFrame();
        ret = false;
    }

    std::this_thread::yield();

    return ret;
}

void Decoder::threadHandleMsg(const ThreadMessageBase &msg)
{
    switch (msg.getType())
    {
    case EMsgType::USER_QUIT:
        break;
    case EMsgType::FOV_CHANGE:
        break;
    }
}

void Decoder::threadEnd()
{
    mInputQ->flush();
    mInputQ->discardAllData();
}

const unique_ptr_AVFrame &Decoder::currentFrame() const
{
    return mpLastFrame;
}

std::shared_ptr<AVCodecContext> Decoder::codecCtx() const
{
    return mpCodecCtx;
}
