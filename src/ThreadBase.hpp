/**
 * @file ThreadBase.hpp
 * @author Jean-Loup Beaussart
 * @class ThreadBase
 * @brief An abstract base class to run some piece of code in a thread. It provides a locking mechanism.
 */

#pragma once

#include "ConcurrentQueue.hpp"
#include "ThreadMessageBase.hpp"
#include "ThreadRegistry.hpp"
#include <mutex>
#include <thread>

class ThreadBase {
private:
    std::thread mThread;
    std::shared_ptr<std::mutex> mpMutex;
    std::mutex mJoinMutex;
    std::string mName;
    ConcurrentQueue<ThreadMessageBase> mIncomingMsgs;

    // let the thread register postMessage() method access our private method post()
    friend bool ThreadRegistry::postMessage(const ThreadMessageBase &, const std::string &) const;

    virtual bool threadBegin() = 0;

    /**
     * @brief Virtual pure method that should implement the work of the thread.
     * This method is called in a loop until it returns false.
     * @return false to stop thread, true to continue
     */
    virtual bool threadWork() = 0;

    virtual void threadHandleMsg(const ThreadMessageBase &msg) = 0;

    virtual void threadEnd() = 0;

    /**
     * @brief Does some internal init stuff and calls threadLoop()
     */
    void init();

    void loop();

    void post(std::unique_ptr<ThreadMessageBase> pMsg);

    void checkMessages();

protected:
    /**
     * @brief Acquires the associated mutex and returns corresponding lock.
     *
     * The lock has to be acquired before accessing shared data.
     * @return a unique lock that hold the thread mutex
     */
    std::unique_lock<std::mutex> getLock() const;

public:
    /**
     * @brief Deleted copy constructor.
     */
    ThreadBase(const ThreadBase &) = delete;

    /**
     * @brief Deleted assignment operator.
     */
    ThreadBase &operator=(const ThreadBase &) = delete;

    ThreadBase(ThreadBase &&) = delete;

    ThreadBase &operator=(ThreadBase &&) = delete;
    /**
     * @brief Constructs a new thread base object.
     * @param threadName a name to identify the thread
     */
    explicit ThreadBase(const std::string &threadName);

    /**
     * @brief Trivial virtual destructor.
     */
    virtual ~ThreadBase() = default;

    /**
     * @brief Shares our mutex with another thread base object.
     *
     * Useful to protect a resource shared between 2 objects of this class.
     * Should be called before any of the 2 threads has started.
     * @param target the thread base object to share our mutex with
     */
    void shareMutexWith(ThreadBase &target) const;

    /**
     * @brief Starts the thread.
     *
     * The thread first executes threadBegin() then threadWork() in a loop and finally threadEnd().
     * ThreadHandleMsg() is called between 2 calls to threadWork() if messages are available.
     */
    void startThread();

    /**
     * @brief Waits for the thread to end.
     *
     * If thread hasn't be started this method returns immediately.
     * @return true if the thread has been joined, false otherwise (thread not started or already joined)
     */
    bool waitThread();

    const std::string &getThreadName() const;
};
