#include "ThreadBase.hpp"
#include "LoguruLogger.hpp"
#include <iostream>

ThreadBase::ThreadBase(const std::string &threadName)
    : mpMutex(std::make_shared<std::mutex>()),
      mName(threadName)
{
}

void ThreadBase::shareMutexWith(ThreadBase &target) const
{
    CHECK_S(!mThread.joinable() && !target.mThread.joinable()) << "Cannot share mutex (from '" << mName << "') with another thread ('" << target.mName << "') after threads have started";
    target.mpMutex = mpMutex;
}

void ThreadBase::startThread()
{
    CHECK_S(!mThread.joinable()) << "Cannot start thread '" << mName << "': thread is already running";
    mThread = std::thread(&ThreadBase::init, this);
}

void ThreadBase::init()
{
    loguru::set_thread_name(mName.c_str());
    loop();
    LOG_S(INFO) << "Thread '" << mName << "' terminated";
}

void ThreadBase::loop()
{
    threadBegin();

    while (threadWork())
        checkMessages();

    threadEnd();
}

void ThreadBase::post(std::unique_ptr<ThreadMessageBase> pMsg)
{
    mIncomingMsgs.tryPut(std::move(pMsg));
}

void ThreadBase::checkMessages()
{
    while (mIncomingMsgs.nbElements() > 0)
    {
        threadHandleMsg(*mIncomingMsgs.get());
    }
}

bool ThreadBase::waitThread()
{
    auto lck = std::unique_lock<std::mutex>(mJoinMutex);

    if (mThread.joinable())
    {
        mThread.join();
        return true;
    }

    return false;
}

std::unique_lock<std::mutex> ThreadBase::getLock() const
{
    return std::unique_lock(*mpMutex);
}

const std::string &ThreadBase::getThreadName() const
{
    return mName;
}
