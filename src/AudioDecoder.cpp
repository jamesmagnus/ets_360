#include "AudioDecoder.hpp"
#include "ConcurrentQueue.hpp"
#include "LoguruLogger.hpp"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>
}

const AVSampleFormat AudioDecoder::scOutAudioFormat  = AV_SAMPLE_FMT_S16;
const uint32_t AudioDecoder::scOutAudioChannelLayout = AV_CH_LAYOUT_STEREO;

AudioDecoder::AudioDecoder(std::shared_ptr<AVCodecContext> pCtx, std::unique_ptr<ConcurrentQueue<Packet>> &pInputQ, std::unique_ptr<ConcurrentQueue<Sample>> &pQOut)
    : Decoder(std::move(pCtx), pInputQ, "audio decoder"),
      mpResampleCtx(swr_alloc(), [](SwrContext *p) { swr_free(&p); }),
      mpQSamples(pQOut)
{
    int64_t channelLayout = static_cast<int64_t>(codecCtx()->channel_layout);

    if (channelLayout != 0)
        channelLayout = scOutAudioFormat;

    av_opt_set_channel_layout(mpResampleCtx.get(), "in_channel_layout", channelLayout, 0);
    av_opt_set_channel_layout(mpResampleCtx.get(), "out_channel_layout", scOutAudioChannelLayout, 0);
    av_opt_set_int(mpResampleCtx.get(), "in_sample_rate", codecCtx()->sample_rate, 0);
    av_opt_set_int(mpResampleCtx.get(), "out_sample_rate", codecCtx()->sample_rate, 0);
    av_opt_set_sample_fmt(mpResampleCtx.get(), "in_sample_fmt", codecCtx()->sample_fmt, 0);
    av_opt_set_sample_fmt(mpResampleCtx.get(), "out_sample_fmt", scOutAudioFormat, 0);

    CHECK_S(swr_init(mpResampleCtx.get()) == 0);
}

bool AudioDecoder::consumeFrame()
{
    if (!currentFrame())
    {
        LOG_S(INFO) << "No more audio frame to decode";
        mpQSamples->flush();
        return false;
    }

    int nbSamples          = currentFrame()->nb_samples;
    size_t outDataSize     = sComputeSampleSize(nbSamples);
    auto pSamples          = std::make_unique<Sample>(outDataSize, swr_next_pts(mpResampleCtx.get(), currentFrame()->pts));
    int nbSamplesConverted = swr_convert(mpResampleCtx.get(), pSamples->raw(), nbSamples, const_cast<const uint8_t **>(currentFrame()->data), nbSamples);

    if (nbSamplesConverted != nbSamples)
        outDataSize = sComputeSampleSize(nbSamplesConverted);

    pSamples->setSize(outDataSize);
    pSamples->setNbSamples(nbSamplesConverted);
    return mpQSamples->tryPut(std::move(pSamples));
}

size_t AudioDecoder::sComputeSampleSize(int nbSamples)
{
    return static_cast<size_t>(av_samples_get_buffer_size(nullptr, av_get_channel_layout_nb_channels(scOutAudioChannelLayout), nbSamples, scOutAudioFormat, 0));
}
