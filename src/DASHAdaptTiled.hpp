/**
 * @file DASHAdaptTiled.hpp
 * @author Jean-Loup Beaussart
 * @class DASHAdaptTiled
 * @brief Implementation of DASH adaptation logic for tiled videos
 *
 * This class implements adaptation logic for tiled video (360).
 */

#pragma once

#include "DASHAdapt.hpp"
#include <map>

struct VideoData;

class DASHAdaptTiled : public DASHAdapt {
private:
    const VideoData &mVideoData;
    std::pair<uint64_t, uint64_t> mViewportCenter;

    bool logic() override;
    void initLogic() override;
    std::map<uint64_t, std::string> representationsForTile(uint64_t tile) const;
    std::vector<uint64_t> tilesInFOV() const;


public:
    DASHAdaptTiled(const DASHAdaptTiled &) = delete;
    DASHAdaptTiled &operator=(const DASHAdaptTiled &) = delete;
    DASHAdaptTiled(DASHAdaptTiled &&)                 = delete;
    DASHAdaptTiled &operator=(DASHAdaptTiled &&) = delete;

    DASHAdaptTiled(std::vector<std::reference_wrapper<NamedPipe<unsigned char>>> &pipes, const VideoData &videoData);

    ~DASHAdaptTiled() override = default;
};
