/**
 * @file AudioDecoder.hpp
 * @author Jean-Loup Beaussart
 * @class AudioDecoder
 * @brief A threaded audio decoder.
 *
 * This decoder gets decoded audio frames from the ffmpeg library, re-samples them and puts them in an out queue.
 * It works in its own thread.
 */

#pragma once

#include "Decoder.hpp"
#include "Sample.hpp"

extern "C" {
#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable : 4244)
#endif

#include <libswresample/swresample.h>

#ifdef _WIN32
#pragma warning(pop)
#endif
}

class AudioDecoder : public Decoder {
private:
    static const AVSampleFormat scOutAudioFormat;
    static const uint32_t scOutAudioChannelLayout;
    std::unique_ptr<SwrContext, void (*)(SwrContext *)> mpResampleCtx;
    std::unique_ptr<ConcurrentQueue<Sample>> &mpQSamples;

    bool consumeFrame() override;
    static size_t sComputeSampleSize(int nbSamples);

public:
    /**
     * @brief Deleted copy constructor.
     */
    AudioDecoder(const AudioDecoder &) = delete;

    /**
     * @brief Deleted assignment operator.
     */
    AudioDecoder &operator=(const AudioDecoder &) = delete;

    AudioDecoder(AudioDecoder &&) = delete;

    AudioDecoder &operator=(AudioDecoder &&) = delete;
    /**
     * @brief Constructs a new audio decoder linked to the given codec.
     *
     * @param pInputQ an input queue with audio packets inside
     * @param pCtx address of the audio codec context
     * @param pQOut an output queue to put the decoded samples in
     */
    AudioDecoder(std::shared_ptr<AVCodecContext> pCtx, std::unique_ptr<ConcurrentQueue<Packet>> &pInputQ, std::unique_ptr<ConcurrentQueue<Sample>> &pQOut);

    /**
     * @brief Destructor, frees allocated resources.
     */
    ~AudioDecoder() override = default;
};
