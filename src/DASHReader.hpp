/**
 * @file DASHReader.hpp
 * @author Jean-Loup Beaussart
 * @class DASHReader
 * @brief A DASH manifest reader/parser with segment download feature
 *
 * This class reads the DASH xml manifest and store all relevant information (video duration, segment size and duration, available qualities, ...).
 * It permits to change current quality and to download the next segment(s).
 * This relies mainly on the libdash library.
 */

#pragma once

#include "Segment.hpp"
#include <deque>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace dash {
class IDASHManager;
namespace mpd {
    class IMPD;
    class IPeriod;
    class IAdaptationSet;
}    // namespace mpd
}    // namespace dash

class DASHReader {
private:
    dash::IDASHManager *mpDashMng{nullptr};
    dash::mpd::IMPD *mpMpd{nullptr};
    dash::mpd::IPeriod *mpCurrentPeriod{nullptr};
    dash::mpd::IAdaptationSet *mpCurrentAdaptation{nullptr};
    std::unique_ptr<Segment> mpInitSeg{nullptr};
    std::deque<std::unique_ptr<Segment>> mSegmentBuffer;
    std::map<uint64_t, std::string> mBandwidths;
    uint64_t mCurrentBandwidth{0};
    uint64_t mNbPeriods{0};
    uint64_t mNbAdaptations{0};
    uint64_t mNbRepresentations{0};
    uint32_t mNbSegments{0};
    uint32_t mSegmentCounter{1};
    uint64_t mLastNetworkMeasure{0};
    double mDurationMs{0};
    double mSegmentDurationMs{0};

    void parsePeriods();
    void parseAdaptationSet();
    void parseRepresentations();
    void parseCurrentPeriodDuration();
    void computeSegmentInformation();
    bool incOrDecQ(bool more);

public:
    DASHReader();

    void open(std::string const &url);

    bool downloadNextSegment();

    bool downloadSegment(uint32_t n);

    uint32_t nextSegmentNumber() const;

    double segmentDuration() const;

    std::unique_ptr<std::vector<unsigned char> const> nextSegmentData();

    std::unique_ptr<std::vector<unsigned char> const> initialSegmentData() const;

    void changeRepresentation(uint64_t newBandwidth);

    uint64_t representationBitrate() const;

    uint64_t representationBitrate(const std::string &ID) const;

    std::string representationID() const;

    const std::map<uint64_t, std::string> &listRepresentations() const;

    uint64_t lastBandwidthMeasure() const;

    void maxQ();

    void minQ();

    bool moreQ();

    bool lessQ();
};
