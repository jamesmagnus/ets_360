#pragma once

#include <memory>

enum class EMsgType { USER_QUIT,
                      FOV_CHANGE };

class ThreadMessageBase {
private:
    EMsgType mType;

public:
    explicit ThreadMessageBase(EMsgType type);

    ThreadMessageBase(const ThreadMessageBase &) = default;

    ThreadMessageBase &operator=(const ThreadMessageBase &) = default;

    ThreadMessageBase(ThreadMessageBase &&) = default;

    ThreadMessageBase &operator=(ThreadMessageBase &&) = default;

    virtual ~ThreadMessageBase() = default;

    EMsgType getType() const;

    virtual std::unique_ptr<ThreadMessageBase> allocateCopy() const = 0;

    virtual size_t size() const = 0;
};
