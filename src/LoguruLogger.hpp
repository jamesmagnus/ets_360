#pragma once

#define LOGURU_WITH_STREAMS 1
#define LOGURU_UNSAFE_SIGNAL_HANDLER 1

#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable : 4018)
#pragma warning(disable : 4996)
#pragma warning(disable : 4100)
#endif

#include "loguru.hpp"

#ifdef _WIN32
#pragma warning(pop)
#endif
