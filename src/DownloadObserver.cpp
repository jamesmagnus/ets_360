#include "DownloadObserver.hpp"
#include "LoguruLogger.hpp"

void DownloadObserver::OnDownloadRateChanged(uint64_t bytesDownloaded)
{
    mNbBytesLastCall = bytesDownloaded;
}

void DownloadObserver::OnDownloadStateChanged(dash::network::DownloadState state)
{
    namespace net = dash::network;

    switch (state)
    {
    case net::NOT_STARTED:
        break;
    case net::IN_PROGRESS:
        mBeginning = Clock::now();
        break;
    case net::REQUEST_ABORT:
        LOG_S(INFO) << "Segment download abort";
        break;
    case net::ABORTED:
        LOG_S(INFO) << "Segment download aborted";
        break;
    case net::COMPLETED:
        mEnd       = Clock::now();
        mCompleted = true;
        break;
    }
}

uint64_t DownloadObserver::averageDLSpeedBps() const
{
    using namespace std::chrono_literals;

    std::chrono::microseconds duration;

    if (mEnd.time_since_epoch().count() != 0)
        duration = std::chrono::duration_cast<decltype(duration)>(mEnd - mBeginning);
    else
        duration = std::chrono::duration_cast<decltype(duration)>(Clock::now() - mBeginning);

    if (duration.count() != 0)
        return (mNbBytesLastCall * 8 * std::chrono::microseconds(1s).count()) / static_cast<uint64_t>(duration.count());

    return 0;
}

bool DownloadObserver::isComplete() const
{
    return mCompleted;
}

uint64_t DownloadObserver::totalSize() const
{
    if (isComplete())
        return mNbBytesLastCall;

    return 0;
}
