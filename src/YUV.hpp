/**
 * @file YUV.hpp
 * @author Jean-Loup Beaussart
 * @class YUV
 * @brief Class representing a decoded video frame.
 * 
 * This class holds frame buffers for Y U and V components and the PTS for sync purpose.
 */

#pragma once

#include <memory>

extern "C" {
#include <libavutil/pixfmt.h>
}

class YUV {
private:
    static const uint32_t scY = 0;
    static const uint32_t scU = 1;
    static const uint32_t scV = 2;
    std::unique_ptr<std::array<unsigned char *, 4>, void (*)(std::array<unsigned char *, 4> *)> mppManagedData;
    size_t mSize;
    int mpLinesize[4];
    const int64_t mcPTS;

    unsigned char *getPlan(uint32_t plan) const;
    int getLinesize(uint32_t plan) const;

public:
    YUV(const YUV &) = delete;
    YUV &operator=(const YUV &) = delete;
    YUV(YUV &&)                 = delete;
    YUV &operator=(YUV &&) = delete;
    YUV(int w, int h, AVPixelFormat pixFmt, int align, int64_t pts);

    ~YUV() = default;

    unsigned char **data() const;

    unsigned char *YData() const;

    unsigned char *UData() const;

    unsigned char *VData() const;

    int const *linesize() const;

    int YSize() const;

    int USize() const;

    int VSize() const;

    size_t size() const;

    int64_t pts() const;
};
