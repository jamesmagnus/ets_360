/**
 * @file NamedPipe.hpp
 * @author Jean-Loup Beaussart
 * @class NamedPipe
 * @brief Template class that represents a named pipe or FIFO file (the implementation depends on the OS)
 *
 * This pipe can be opened like a file but data inside arise from memory.
 * In Linux this class is just a wrapper around a FIFO file.
 * In Windows it uses a named pipe which can be opened like a file.
 * Objects of this class become owner of the data you pass to them.
 * Once copied on the underlying file/pipe data are freed.
 * Writing occurs in a thread so when adding data to the NamedPipe the method returns immediatly even if the underlying pipe/file is full.
 * @tparam T the type of data the pipe should transfer (i.e. unsigned char or int).
 */

#pragma once

#include "ThreadBase.hpp"
#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <experimental/filesystem>
#include <fstream>
#include <vector>

#ifdef _WIN32
#include <windows.h>
#else
extern "C" {
#include <sys/stat.h>
}
#endif

template <class T>
class NamedPipe : public ThreadBase {
private:
#ifdef _WIN32
    HANDLE mPipeHandle;
#else
    std::ofstream mFile;
#endif
    std::vector<std::unique_ptr<std::vector<T> const>> mData;
    std::string mFilename;
    std::condition_variable mCond;
    std::atomic_bool mIsFIFODeleted{false};
    bool mEnd{false};

    bool threadBegin() override
    {
#ifndef _WIN32
        mFile.open(mFilename, std::fstream::binary);
#endif
        return true;
    }

    bool threadWork() override
    {
        using namespace std::chrono_literals;

        auto lck = getLock();

        for (auto segIt = mData.begin(), end = mData.end(); segIt != end; ++segIt)
        {
            auto it    = (*segIt)->cbegin();
            auto pData = reinterpret_cast<const char *>(&(*it));
#ifdef _WIN32
            DWORD nb = 0;
            while (!WriteFile(mPipeHandle, pData, sizeof(T) * (*segIt)->size(), &nb, NULL))
                if (GetLastError() != 536)    // waiting reader to open the other end
                {
                    LOG_S(ERROR) << "Error " << GetLastError() << " when trying to write in named pipe";
                    break;
                }
#else
            mFile.write(pData, static_cast<std::streamsize>(sizeof(T) * (*segIt)->size()));
            mFile.flush();
#endif
            *segIt = nullptr;
        }

        mData.erase(std::remove(mData.begin(), mData.end(), nullptr), mData.end());

        mCond.wait_for(lck, 50ms, [this]() {
            return !mData.empty() || mEnd;
        });

        return !(mEnd && mData.empty());
    }

    void threadHandleMsg(const ThreadMessageBase &msg) override
    {
        switch (msg.getType())
        {
        case EMsgType::USER_QUIT:
        {
            auto lck = getLock();
            mEnd     = true;
            mData.clear();    // if user quits just discard remaining data
            break;
        }
        case EMsgType::FOV_CHANGE:
            break;
        }
    }

    void threadEnd() override
    {
        mIsFIFODeleted = true;
#ifndef _WIN32
        mFile.close();
#endif
        std::remove(mFilename.c_str());
    }

public:
    NamedPipe(const NamedPipe &) = delete;
    NamedPipe &operator=(const NamedPipe &) = delete;
    NamedPipe(NamedPipe &&)                 = delete;
    NamedPipe &operator=(NamedPipe &&) = delete;

    NamedPipe()
        : ThreadBase("Pipe")
    {
    }

    ~NamedPipe() override = default;

    std::string create(std::string const &pipeName)
    {
#ifdef _WIN32
        mFilename = "\\\\.\\pipe\\" + pipeName;
#else
        mFilename = std::experimental::filesystem::temp_directory_path() / pipeName;
#endif

#ifdef _WIN32
        mPipeHandle = CreateNamedPipeA(mFilename.c_str(), PIPE_ACCESS_OUTBOUND, PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT, 1, 2048, 2048, 200, NULL);
        if (mPipeHandle == INVALID_HANDLE_VALUE)
        {
#else
        if (!std::experimental::filesystem::exists(mFilename))
        {
            if (mkfifo(mFilename.c_str(), 0600) != 0)
#endif
            throw std::runtime_error("Cannot create fifo");
        }
        startThread();

        return mFilename;
    }

    void setInputData(std::unique_ptr<std::vector<T> const> pData)
    {
        auto lck = getLock();

        if (!pData)
        {
            mEnd = true;
            lck.unlock();
            mCond.notify_one();

            return;
        }

        mData.push_back(std::move(pData));
        lck.unlock();
        mCond.notify_one();
    }

    void emptyPipe()
    {
        if (mIsFIFODeleted)
            return;

        std::ifstream pipeStream;
        pipeStream.open(mFilename, std::ios::binary);
        if (pipeStream.is_open() && pipeStream.good())
            pipeStream.ignore(std::numeric_limits<std::streamsize>::max());
    }
};
