#include "DASHAdaptTiled.hpp"
#include "DASHReader.hpp"
#include "LoguruLogger.hpp"
#include "NamedPipe.hpp"
#include "VideoData.hpp"
#include <regex>

DASHAdaptTiled::DASHAdaptTiled(std::vector<std::reference_wrapper<NamedPipe<unsigned char>>> &pipes, const VideoData &videoData)
    : DASHAdapt(pipes),
      mVideoData(videoData),
      mViewportCenter(std::make_pair(1, 1))
{
}

bool DASHAdaptTiled::logic()
{
    try
    {
        auto availableBandwidth = reader().lastBandwidthMeasure();
        auto tilesHighQ    = tilesInFOV();
        uint32_t segNumber = reader().nextSegmentNumber();
        LOG_S(INFO) << "Download seg " << segNumber;
        
        // Low quality
        for (uint64_t tile = 0; tile < mVideoData.nbTiles(); ++tile)
        {
            auto rep = representationsForTile(tile);
            auto min = rep.begin();

            if (std::find(tilesHighQ.cbegin(), tilesHighQ.cend(), tile) == tilesHighQ.cend())
                reader().changeRepresentation(min->first);
            else
                continue;
                
            reader().downloadSegment(segNumber);
            availableBandwidth -= reader().representationBitrate();
        }
        
        availableBandwidth /= tilesHighQ.size();
        
        // High quality
        for (uint64_t tile : tilesHighQ)
        {
            auto rep = representationsForTile(tile);
            auto greatestThatFit    = rep.lower_bound(availableBandwidth);

            // Value returned by lower_bound() is the first >= availableBandwidth, so we take the representation just before it
            if (greatestThatFit != rep.cbegin())
                greatestThatFit--;

            reader().changeRepresentation(greatestThatFit->first);
                
            reader().downloadSegment(segNumber);
        }
        
        // Wait for download to complete and write data to pipe
        for (uint64_t tile = 0; tile < mVideoData.nbTiles(); ++tile)
        {
            auto pData = reader().nextSegmentData();

            if (!pData)
                return false;

            pipe(tile).setInputData(std::move(pData));
        }
    }
    catch (std::exception &e)
    {
        LOG_S(ERROR) << "Cannot download segment: " << e.what();
    }

    using namespace std::chrono_literals;
    std::this_thread::sleep_for(700ms); //TODO compute real timing

    return true;
}

void DASHAdaptTiled::initLogic()
{
}

std::map<uint64_t, std::string> DASHAdaptTiled::representationsForTile(uint64_t tile) const
{
    auto representations = reader().listRepresentations();
    decltype(representations) validRepresentations;
    auto nbTileX    = static_cast<uint64_t>(mVideoData.tileX);
    auto tileIndexX = tile / nbTileX + 1;
    auto tileIndexY = tile % nbTileX + 1;
    std::stringstream ss;
    ss << ".*tile_" << tileIndexX << "_" << tileIndexY << ".*";
    std::regex targetTile(ss.str(), std::regex::ECMAScript);
    std::cmatch cm;
    auto it = representations.cbegin();

    while (it != representations.cend())
    {
        if (std::regex_match(it->second.c_str(), cm, targetTile))
        {
            validRepresentations.insert(*it);
        }
        ++it;
    }

    return validRepresentations;
}

// TODO really compute tiles in FOV
std::vector<uint64_t> DASHAdaptTiled::tilesInFOV() const
{
    std::vector<uint64_t> FOVTiles;
    auto nbTileX = static_cast<uint64_t>(mVideoData.tileX);
    auto nbTileY = static_cast<uint64_t>(mVideoData.tileY);


    for (auto x = mViewportCenter.first; x <= mViewportCenter.first + 1; ++x)
        for (auto y = mViewportCenter.second; y <= mViewportCenter.second + 1; ++y)
            if (x < nbTileX && y < nbTileY)
                FOVTiles.push_back(x * nbTileX + y);

    return FOVTiles;
}
