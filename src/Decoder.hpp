/**
 * @file Decoder.hpp
 * @author Jean-Loup Beaussart
 * @class Decoder
 * @brief An abstract decoder base class.
 */
#pragma once

#include "Packet.hpp"
#include "ThreadBase.hpp"

struct AVCodecContext;
struct AVFrame;

template <class T>
class ConcurrentQueue;

using unique_ptr_AVFrame = std::unique_ptr<AVFrame, void (*)(AVFrame *)>;

class Decoder : public ThreadBase {
private:
    std::shared_ptr<AVCodecContext> mpCodecCtx;
    unique_ptr_AVFrame mpLastFrame;
    std::unique_ptr<ConcurrentQueue<Packet>> &mInputQ;

    /**
     * @brief Called when a new frame is ready.
     * This method should be implemented in derived classes to handle the frame.
     */
    virtual bool consumeFrame() = 0;

    bool threadBegin() override;
    /**
     * @brief Continuously pulls the ffmpeg decoder to get the frames.
     * This method extracts each frame from the ffmpeg decoder and calls consumeFrame().
     * It loops until the decoder get flushed.
     */
    bool threadWork() override;

    void threadHandleMsg(const ThreadMessageBase &msg) override;

    void threadEnd() override;

protected:
    /**
     * @brief Returns the address of the last decoded frame.
     * @return address of last frame
     */
    const unique_ptr_AVFrame &currentFrame() const;

    /**
     * @brief Returns the codec context address.
     * @return address of codec context
     */
    std::shared_ptr<AVCodecContext> codecCtx() const;

public:
    /**
     * @brief Deleted copy constructor.
     */
    Decoder(const Decoder &) = delete;

    /**
     * @brief Deleted assignment operator.
     */
    Decoder &operator=(const Decoder &) = delete;

    Decoder(Decoder &&) = delete;

    Decoder &operator=(Decoder &&) = delete;

    /**
     * @brief Constructs a new decoder from a codec.
     * @param pCtx address of the codec context
     */
    Decoder(std::shared_ptr<AVCodecContext> pCtx, std::unique_ptr<ConcurrentQueue<Packet>> &inputQ, const std::string &threadName);

    /**
     * Destructor, frees allocated resources.
     */
    ~Decoder() override = default;
};
