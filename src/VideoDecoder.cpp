#include "VideoDecoder.hpp"
#include "ConcurrentQueue.hpp"

extern "C" {
#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable : 4244)
#endif

#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>

#ifdef _WIN32
#pragma warning(pop)
#endif
}

const AVPixelFormat VideoDecoder::scOutVideoPixFormat = AV_PIX_FMT_YUV420P;
const int VideoDecoder::scAlign                       = 32;

VideoDecoder::VideoDecoder(std::shared_ptr<AVCodecContext> pCtx, std::unique_ptr<ConcurrentQueue<Packet>> &inputQ, std::unique_ptr<ConcurrentQueue<YUV>> &pQOut)
    : Decoder(std::move(pCtx), inputQ, "video decoder"),
      mpSwsCtx(nullptr, [](SwsContext *p) { sws_freeContext(p); }),
      mpQYUV(pQOut)
{
    mpSwsCtx.reset(sws_getContext(codecCtx()->width, codecCtx()->height, codecCtx()->pix_fmt, codecCtx()->width, codecCtx()->height, scOutVideoPixFormat, SWS_BILINEAR, nullptr, nullptr, nullptr));
}

bool VideoDecoder::consumeFrame()
{
    if (!currentFrame())
    {
        LOG_S(INFO) << "No more video frame to decode";
        mpQYUV->flush();
        return false;
    }

    int w     = codecCtx()->width;
    int h     = codecCtx()->height;
    auto pts  = static_cast<uint64_t>(currentFrame()->pts);
    auto pYUV = std::make_unique<YUV>(w, h, scOutVideoPixFormat, scAlign, pts);

    sws_scale(mpSwsCtx.get(), currentFrame()->data, currentFrame()->linesize, 0, h, pYUV->data(), pYUV->linesize());

    return mpQYUV->tryPut(std::move(pYUV));
}
