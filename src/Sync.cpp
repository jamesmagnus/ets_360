#include "Sync.hpp"
#include "Clock.hpp"
#include "ConcurrentQueue.hpp"
#include "SDLRect.hpp"
#include "SDLSystem.hpp"
#include "ThreadMessage.hpp"
#include <algorithm>

Sync::Sync(std::unique_ptr<SDL::System> &pSDLSystem, std::vector<std::unique_ptr<ConcurrentQueue<YUV>>> &pVideoQs, std::unique_ptr<ConcurrentQueue<Sample>> &pAudioQ, const VideoData &cVideoData)
    : ThreadBase("sync and display"),
      mcSyncThreshold(10),
      mVideoData(cVideoData),
      mpYUVQs(pVideoQs),
      mpSampleQ(pAudioQ),
      mpSDLSys(pSDLSystem),
      mViewport(mpSDLSys->renderWidth(), mpSDLSys->renderHeight(), 80.0f, 60.0f),
      mQuit(false)
{
    CHECK_S(mVideoData.nbTiles() == mpYUVQs.size()) << "The number of raw video queues (" << mpYUVQs.size() << ") doesn't match the number of tiles (" << mVideoData.nbTiles() << ")";

    mpLastFrame.resize(mVideoData.nbTiles());

    int tileSizeX = mVideoData.w / mVideoData.tileX;
    int tileSizeY = mVideoData.h / mVideoData.tileY;

    for (uint64_t i = 0; i < mVideoData.nbTiles(); ++i)
        mSDLScreens.push_back(mpSDLSys->newTexture(SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, tileSizeX, tileSizeY));
}

bool Sync::threadBegin()
{
    return true;
}

bool Sync::threadWork()
{
    using namespace std::chrono_literals;

    auto pSample = mpSampleQ->get();
    if (pSample)
    {
        auto pts  = mpcClock->audioPtsToMs(pSample->pts());
        auto diff = pts - mpcClock->timeInMs();

        if (diff > mcSyncThreshold)
            std::this_thread::sleep_for(diff);
        else if (diff < -mcSyncThreshold)
        {
            LOG_S(WARNING) << "Sample is late by " << -diff.count() << " ms (PTS: " << pts.count() << " vs clock: " << mpcClock->timeInMs().count() << ")";
            return true;
        }

        mpSDLSys->feedAudio(std::move(pSample));
    }
    else
        return false;

    return !mQuit;
}

void Sync::threadHandleMsg(const ThreadMessageBase &msg)
{
    switch (msg.getType())
    {
    case EMsgType::USER_QUIT:
        break;
    case EMsgType::FOV_CHANGE:
        break;
    }
}

void Sync::threadEnd()
{
    mpSampleQ->flush();
    mpSampleQ->discardAllData();
}

void Sync::attachPlaybackClock(std::shared_ptr<const Clock> pcClock)
{
    mpcClock = std::move(pcClock);
}

int64_t Sync::pullFrameTiles()
{
    using namespace std::chrono_literals;

    auto begin = mpcClock->timeInMs();
    std::vector<bool> tilesAreDecoded(mVideoData.nbTiles());
    int64_t pts = -1;

    auto threshold = begin + 35ms;

    while (mpcClock->timeInMs() <= threshold && !areAllTrue(tilesAreDecoded))
    {
        for (uint64_t tile = 0; tile < mVideoData.nbTiles(); ++tile)
        {
            if (tilesAreDecoded[tile])
                continue;

            std::unique_ptr<YUV> pYUV = mpYUVQs[tile]->get(1ms);

            if (pYUV)
            {
                if (pts != -1 && pts != pYUV->pts())
                    if (pYUV->pts() <= pts)
                    {
                        LOG_S(WARNING) << "Dropping tile " << tile << " parts of an old frame";
                        continue;
                    }

                pts                   = pYUV->pts();
                mpLastFrame[tile]     = std::move(pYUV);
                tilesAreDecoded[tile] = true;
            }
        }
        if (pts != -1)
            threshold = mpcClock->videoPtsToMs(pts);
    }

    if (!areAllTrue(tilesAreDecoded))
        LOG_S(WARNING) << "Cannot get all tiles of next frame";

    return pts;
}

void Sync::updateLoop()
{
    // auto test = mViewport.viewportBound(mVideoData.w, mVideoData.h);

    // for (auto vec : test)
    // {
    //     LOG_S(INFO) << vec(0) << "," << vec(1);
    // }

    while (!mQuit)
    {
        int64_t pts = pullFrameTiles();

        for (uint64_t tile = 0; tile < mVideoData.nbTiles(); ++tile)
        {
            if (mpLastFrame[tile])
                updateTile(mpLastFrame[tile], tile);

            if (pts == -1)
            {
                if (areQsFlushed())
                {
                    mQuit = true;
                    break;
                }

                LOG_S(ERROR) << "No frame available in output queues!";
                std::this_thread::yield();
            }
        }

        syncAndDisplay(mpcClock->videoPtsToMs(pts));

        SDL_Event event;
        SDL_PollEvent(&event);
        switch (event.type)
        {
        case SDL_QUIT:
            mQuit = true;
            break;
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym)
            {
            case SDLK_ESCAPE:
                mQuit = true;
                break;
            }
            break;
        default:
            break;
        }
    }

    ThreadMessage<> userExitMsg(EMsgType::USER_QUIT);
    ThreadRegistry::get().postMessage(userExitMsg);

    flushAllQs();
    discardAllQs();
}

void Sync::flushAllQs()
{
    for (auto &q : mpYUVQs)
        q->flush();
}

void Sync::discardAllQs()
{
    for (auto &q : mpYUVQs)
        q->discardAllData();
}

bool Sync::areQsFlushed() const
{
    for (auto &q : mpYUVQs)
        if (!q->isFlushed())
            return false;
    return true;
}

void Sync::syncAndDisplay(std::chrono::milliseconds pts)
{
    int tileSizeX = mVideoData.w / mVideoData.tileX;
    int tileSizeY = mVideoData.h / mVideoData.tileY;
    SDL::Rect null;

    mpSDLSys->renderClear();

    for (int x = 0; x < mVideoData.tileX; ++x)
        for (int y = 0; y < mVideoData.tileY; ++y)
        {
            int tile = y * mVideoData.tileX + x;
            SDL::Rect targetTile(x * tileSizeX, y * tileSizeY, tileSizeX, tileSizeY);

            mpSDLSys->renderCopy(mSDLScreens[static_cast<uint64_t>(tile)], null, targetTile);
        }

    auto diff = pts - mpcClock->timeInMs();
    if (diff > mcSyncThreshold)
        std::this_thread::sleep_for(diff);
    else if (diff < -mcSyncThreshold)
        LOG_S(WARNING) << "Frame is late by " << -diff.count() << " ms (PTS: " << pts.count() << " vs clock: " << mpcClock->timeInMs().count() << ")";

    mpSDLSys->renderPresent();
}

void Sync::updateTile(std::unique_ptr<YUV> &pFrame, uint64_t tile)
{
    mSDLScreens[tile].update(pFrame->YData(), pFrame->YSize(), pFrame->UData(), pFrame->USize(), pFrame->VData(), pFrame->VSize());
}

bool Sync::areAllTrue(const std::vector<bool> &bools)
{
    return std::find(bools.cbegin(), bools.cend(), false) == bools.cend();
}
