/**
 * @file VideoData.hpp
 * @author Jean-Loup Beaussart
 * @struct VideoData
 * @brief Just a structure to agregate all the video characteristics.  
 *
 */

#pragma once

#include <string>

struct VideoData {
    int w{0};
    int h{0};
    int tileX{1};
    int tileY{1};
    bool is360{false};
    bool is3D{false};
    bool useHwAccel{false};
    std::string path;

    uint64_t nbTiles() const
    {
        return static_cast<uint64_t>(tileX) * static_cast<uint64_t>(tileY);
    }
};
