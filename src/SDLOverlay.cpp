#include "SDLOverlay.hpp"
#include "LoguruLogger.hpp"
#include "SDLRect.hpp"
#include "SDLSystem.hpp"
#include "SDLTexture.hpp"

namespace SDL {
Overlay::Overlay(std::unique_ptr<System> &pSDLSys, const std::string &font, int size)
    : mpSys(pSDLSys),
      mpFont(nullptr, [](TTF_Font *p) { TTF_CloseFont(p); }),
      mRenderColor({255, 255, 255, SDL_ALPHA_OPAQUE})
{
    CHECK_S(TTF_Init() == 0) << System::printSDLFailure("Cannot init SDL TTF library");

    mpFont.reset(TTF_OpenFont(font.c_str(), size));
    CHECK_S(mpFont != nullptr) << System::printSDLFailure("Cannot load font " + font);
}

Overlay::~Overlay()
{
    mpFont = nullptr;    // font should be freed before uninitializing the library
    TTF_Quit();
}

void Overlay::write(const std::string &msg, Rect &position)
{
    auto pSurf = std::unique_ptr<SDL_Surface, void (*)(SDL_Surface *)>(TTF_RenderText_Blended(mpFont.get(), msg.c_str(), mRenderColor), [](SDL_Surface *p) { SDL_FreeSurface(p); });
    CHECK_S(pSurf != nullptr);

    Texture texture = mpSys->newTexture(std::move(pSurf));
    Rect msgSize(position);
    Rect r;

    CHECK_S(TTF_SizeText(mpFont.get(), msg.c_str(), &msgSize.SDLPtr()->w, &msgSize.SDLPtr()->h) == 0) << System::printSDLFailure("Cannot determinate size of text " + msg);
    mpSys->renderCopy(texture, r, msgSize);
}
}    // namespace SDL
