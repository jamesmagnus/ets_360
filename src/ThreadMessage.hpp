#pragma once

#include "ThreadMessageBase.hpp"
#include <memory>

template <class T = void>
class ThreadMessage : public ThreadMessageBase {
private:
    std::shared_ptr<T> mpPayload;

public:
    ThreadMessage &operator=(const ThreadMessage &) = default;

    ThreadMessage(const ThreadMessage &) = default;

    ThreadMessage(ThreadMessage &&) noexcept = default;

    ThreadMessage &operator=(ThreadMessage &&) noexcept = default;

    template <class... Args>
    explicit ThreadMessage(EMsgType type, Args &&... args)
        : ThreadMessageBase(type),
          mpPayload(std::make_shared<T>(std::forward<Args>(args)...))
    {
    }

    ~ThreadMessage() override = default;

    std::unique_ptr<ThreadMessageBase> allocateCopy() const override
    {
        return std::unique_ptr<ThreadMessage>(new ThreadMessage(*this));
    }

    const T &getPayload() const
    {
        return *mpPayload;
    }

    size_t size() const override
    {
        return sizeof(T);
    }
};

template <>
class ThreadMessage<void> : public ThreadMessageBase {
public:
    ThreadMessage &operator=(const ThreadMessage &) = default;

    ThreadMessage(const ThreadMessage &) = default;

    ThreadMessage(ThreadMessage &&) = default;

    ThreadMessage &operator=(ThreadMessage &&) = default;

    explicit ThreadMessage(EMsgType type)
        : ThreadMessageBase(type)
    {
    }

    ~ThreadMessage() override = default;

    std::unique_ptr<ThreadMessageBase> allocateCopy() const override
    {
        return std::make_unique<ThreadMessage>(*this);
    }

    size_t size() const override
    {
        return 0;
    }
};
