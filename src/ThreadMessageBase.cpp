#include "ThreadMessageBase.hpp"

ThreadMessageBase::ThreadMessageBase(EMsgType type)
    : mType(type)
{
}

EMsgType ThreadMessageBase::getType() const
{
    return mType;
}
