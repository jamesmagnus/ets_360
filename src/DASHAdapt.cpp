#include "DASHAdapt.hpp"
#include "DASHReader.hpp"
#include "LoguruLogger.hpp"
#include "NamedPipe.hpp"

DASHAdapt::DASHAdapt(std::vector<std::reference_wrapper<NamedPipe<unsigned char>>> &pipes)
    : ThreadBase("DASH adapter"),
      mPipes(pipes)
{
}

void DASHAdapt::setReader(std::unique_ptr<DASHReader> pReader)
{
    mpReader = std::move(pReader);
}

void DASHAdapt::attachClock(std::shared_ptr<Clock> &pClock)
{
    mpClock = pClock;
}

bool DASHAdapt::threadBegin()
{
    if (!(!mPipes.empty() && mpReader && mpClock))
    {
        LOG_S(ERROR) << "DASH reader, output pipes or playback clock not set";
        return false;
    }

    for_each(mPipes.begin(), mPipes.end(), [this](NamedPipe<unsigned char> &pipe) {
        pipe.setInputData(mpReader->initialSegmentData());
    });

    initLogic();

    return true;
}

bool DASHAdapt::threadWork()
{
    return !mQuit && logic();
}

void DASHAdapt::threadHandleMsg(const ThreadMessageBase &msg)
{
    switch (msg.getType())
    {
    case EMsgType::USER_QUIT:
        mQuit = true;
        break;
    case EMsgType::FOV_CHANGE:
        break;
    }
}

void DASHAdapt::threadEnd()
{
    for_each(mPipes.begin(), mPipes.end(), [](NamedPipe<unsigned char> &pipe) {
        pipe.setInputData(nullptr);
    });
}

DASHReader &DASHAdapt::reader() const
{
    return *mpReader;
}

const Clock &DASHAdapt::clock() const
{
    return *mpClock;
}

NamedPipe<unsigned char> &DASHAdapt::pipe(uint64_t pipeNum) const
{
    CHECK_S(pipeNum < mPipes.size());
    return mPipes[pipeNum];
}
