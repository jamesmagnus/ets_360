#include "DASHReader.hpp"
#include "LoguruLogger.hpp"
#include "Segment.hpp"
#include <cmath>
#include <libdash.h>
#include <regex>

using namespace dash;

DASHReader::DASHReader()
{
    mpDashMng = CreateDashManager();
}

void DASHReader::open(std::string const &url)
{
    mpMpd = mpDashMng->Open(const_cast<char *>(url.c_str()));
    CHECK_S(mpMpd != nullptr) << "Cannot open " << url;

    LOG_SCOPE_F(INFO, "Information about DASH content:");
    LOG_S(INFO) << "Base URL: " << mpMpd->GetBaseUrls()[0]->GetUrl();

    parsePeriods();
    if (mpCurrentPeriod != nullptr)
        parseAdaptationSet();
    if (mpCurrentAdaptation != nullptr)
    {
        parseRepresentations();
        computeSegmentInformation();
    }
}

void DASHReader::parsePeriods()
{
    auto periods = mpMpd->GetPeriods();
    mNbPeriods   = periods.size();

    CHECK_S(mNbPeriods > 0) << "No period found";
    mpCurrentPeriod = periods[0];
    parseCurrentPeriodDuration();

    LOG_S(INFO) << mNbPeriods << " periods found";
}

void DASHReader::parseAdaptationSet()
{
    auto adaptations = mpCurrentPeriod->GetAdaptationSets();
    mNbAdaptations   = adaptations.size();

    if (mNbAdaptations > 0)
        mpCurrentAdaptation = adaptations[0];
    else
        mpCurrentAdaptation = nullptr;

    LOG_S(INFO) << mNbAdaptations << " adaptations found in period n° " << mpCurrentPeriod->GetId();
}

void DASHReader::parseRepresentations()
{
    auto representations = mpCurrentAdaptation->GetRepresentation();
    mNbRepresentations   = representations.size();
    LOG_S(INFO) << mNbRepresentations << " representations found in adaptation n° " << mpCurrentAdaptation->GetId() << " :";

    for (auto rep : mpCurrentAdaptation->GetRepresentation())
        mBandwidths.insert(std::make_pair(rep->GetBandwidth(), rep->GetId()));

    for (auto pair : mBandwidths)
        LOG_S(INFO) << "\t" << pair.second << " (" << pair.first << ")";

    auto init = mBandwidths.cbegin();

    mCurrentBandwidth = init->first;
    LOG_S(INFO) << "Selecting initiale bandwidth " << init->second << " (" << init->first << ")";
}

void DASHReader::computeSegmentInformation()
{
    LOG_SCOPE_F(INFO, "Segment information for current adaptation:");

    auto segTemplate = mpCurrentAdaptation->GetSegmentTemplate();
    CHECK_S(segTemplate != nullptr);
    mSegmentDurationMs = 1000.0 * static_cast<double>(segTemplate->GetDuration()) / static_cast<double>(segTemplate->GetTimescale());
    mNbSegments        = static_cast<uint32_t>(std::ceil(mDurationMs / mSegmentDurationMs));

    dash::mpd::ISegment *pInitSegment = segTemplate->ToInitializationSegment(mpMpd->GetBaseUrls(), "", 0);
    mpInitSeg                         = std::make_unique<Segment>(*pInitSegment);
    mpInitSeg->startDownload();

    LOG_S(INFO) << "initialization segment: " << segTemplate->Getinitialization();
    LOG_S(INFO) << "duration: " << segTemplate->GetDuration();
    LOG_S(INFO) << "timescale: " << segTemplate->GetTimescale();
    LOG_S(INFO) << "duration in sec: " << mSegmentDurationMs / 1000;
    LOG_S(INFO) << "# of segments: " << mNbSegments;
}

void DASHReader::parseCurrentPeriodDuration()
{
    static const double SECONDS[] = {3.154E7, 2.628E6, 86400.0, 3600.0, 60.0, 1.0, 1.0};
    std::regex iso8601("P(?:([0-9]+)Y)?(?:([0-9]+)M)?(?:([0-9]+)D)?T(?:([0-9]+)H)?(?:([0-9]+)M)?(?:([0-9]+)(.[0-9]+)?S)?", std::regex::ECMAScript);
    std::cmatch match;

    std::regex_match(mpCurrentPeriod->GetDuration().c_str(), match, iso8601);
    for (uint64_t i = 1, j = 0; i < match.size(); ++i, ++j)
    {
        char *res = nullptr;
        if (!std::string(match[i]).empty())
            mDurationMs += SECONDS[j] * 1000.0 * std::strtod(std::string(match[i]).c_str(), &res);
    }
}

bool DASHReader::downloadNextSegment()
{
    if (mSegmentCounter > mNbSegments)
        return false;

    dash::mpd::ISegment *pNextSeg = mpCurrentAdaptation->GetSegmentTemplate()->GetMediaSegmentFromNumber(mpMpd->GetBaseUrls(), mBandwidths[mCurrentBandwidth], static_cast<uint32_t>(mCurrentBandwidth), mSegmentCounter);

    mSegmentBuffer.emplace_back(std::make_unique<Segment>(*pNextSeg));
    mSegmentBuffer.back()->startDownload();
    mSegmentCounter++;

    return true;
}

bool DASHReader::downloadSegment(uint32_t n)
{
    mSegmentCounter = n;
    return downloadNextSegment();
}

uint32_t DASHReader::nextSegmentNumber() const
{
    return mSegmentCounter;
}

double DASHReader::segmentDuration() const
{
    return mSegmentDurationMs;
}

std::unique_ptr<std::vector<unsigned char> const> DASHReader::initialSegmentData() const
{
    auto it = mpInitSeg->read(0, 0);
    return std::make_unique<std::vector<unsigned char> const>(it, it + static_cast<int64_t>(mpInitSeg->totalSize()));
}

void DASHReader::changeRepresentation(uint64_t newBandwidth)
{
    if (mBandwidths.count(newBandwidth) != 0)
        mCurrentBandwidth = newBandwidth;
    else
        LOG_S(ERROR) << "No representation with bandwidth = " << newBandwidth << " found. ";
}

std::unique_ptr<std::vector<unsigned char> const> DASHReader::nextSegmentData()
{
    if (mSegmentBuffer.empty())
        return nullptr;

    auto pData = std::make_unique<std::vector<unsigned char>>();
    auto pSeg  = std::move(mSegmentBuffer.front());
    mSegmentBuffer.pop_front();

    auto segData = pSeg->read(0, 0);

    mLastNetworkMeasure = pSeg->downloadSpeed();

    auto segSize = static_cast<int64_t>(pSeg->totalSize());
    pData->insert(pData->cend(), segData, segData + segSize);

    return pData;
}

uint64_t DASHReader::lastBandwidthMeasure() const
{
    return mLastNetworkMeasure;
}

uint64_t DASHReader::representationBitrate() const
{
    return mCurrentBandwidth;
}

uint64_t DASHReader::representationBitrate(const std::string &ID) const
{
    auto it = mBandwidths.cbegin();

    while (it != mBandwidths.cend())
    {
        if (it->second == ID)
            return it->first;

        ++it;
    }

    throw std::runtime_error("No representation named " + ID + " found");
}

std::string DASHReader::representationID() const
{
    return mBandwidths.at(mCurrentBandwidth);
}

const std::map<uint64_t, std::string> &DASHReader::listRepresentations() const
{
    return mBandwidths;
}

void DASHReader::maxQ()
{
    auto l = ++mBandwidths.crend();
    changeRepresentation(l->first);
}

void DASHReader::minQ()
{
    changeRepresentation(mBandwidths.cbegin()->first);
}

bool DASHReader::incOrDecQ(bool more)
{
    auto bandwidth = mBandwidths.find(mCurrentBandwidth);

    if (bandwidth == mBandwidths.cend())
        return false;

    if (more)
        bandwidth++;
    else if (bandwidth != mBandwidths.cbegin())
        bandwidth--;
    else
        return false;

    if (bandwidth == mBandwidths.cend())
        return false;

    changeRepresentation(bandwidth->first);

    return true;
}

bool DASHReader::moreQ()
{
    return incOrDecQ(true);
}

bool DASHReader::lessQ()
{
    return incOrDecQ(false);
}
