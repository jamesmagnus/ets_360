#include "Sample.hpp"
#include <sstream>

static void dataDeleter(unsigned char **p)
{
    delete[] p[0];
    delete[] p;
}

static unsigned char **dataAllocator(size_t size)
{
    auto ret = new unsigned char *[1];
    ret[0]   = new unsigned char[static_cast<size_t>(size)];
    return ret;
}

Sample::Sample(size_t size, int64_t pts)
    : mppBuffer(dataAllocator(size), dataDeleter),
      mBufferSize(size),
      mNbSamples(0),
      mcPTS(pts)
{
}

unsigned char **Sample::raw() const
{
    return mppBuffer.get();
}

size_t Sample::size() const
{
    return mBufferSize;
}

int Sample::nbSamples() const
{
    return mNbSamples;
}

void Sample::setSize(size_t size)
{
    if (size > mBufferSize)
    {
        std::stringstream err;
        err << "Written data size " << size << " cannot be greater than internal allocated size " << mBufferSize;
        throw std::runtime_error(err.str());
    }
    mBufferSize = size;
}

void Sample::setNbSamples(int n)
{
    mNbSamples = n;
}

int64_t Sample::pts() const
{
    return mcPTS;
}
