/**
 * @file DASHAdapt.hpp
 * @author Jean-Loup Beaussart
 * @class DASHAdapt
 * @brief A abstract base class to implement bandwidth adaptation logic.
 *
 * This class handles the basic stuff of downloading segments through a DASHReader instance and writing the data to output pipes.
 * Derived classes should just implement the adaptation logic (i.e. choose what tiles/segments and quality to download).
 */

#pragma once

#include "ThreadBase.hpp"
#include <atomic>
#include <memory>
#include <vector>

class DASHReader;
template <class T>
class NamedPipe;
class Clock;

class DASHAdapt : public ThreadBase {
private:
    std::unique_ptr<DASHReader> mpReader{nullptr};
    std::vector<std::reference_wrapper<NamedPipe<unsigned char>>> &mPipes;
    std::shared_ptr<Clock> mpClock{nullptr};
    std::atomic_bool mQuit{false};

    virtual void initLogic() = 0;
    virtual bool logic()     = 0;
    bool threadBegin() override;
    bool threadWork() override;
    void threadHandleMsg(const ThreadMessageBase &msg) override;
    void threadEnd() override;

protected:
    /**
     * @brief Returns the associated DASHReader.
     *
     * @return a DASHReader
     */
    DASHReader &reader() const;

    /**
     * @brief Returns the associated playback clock.
     *
     * @return a clock
     */
    const Clock &clock() const;

    /**
     * @brief Returns one of the pipe used to transfer the media data.
     *
     * If the pipe index is out of range the method throws.
     * @param pipeNum the index of the pipe, it corresponds to the tile index
     * @return the NamedPipe corresponding to the given index
     */
    NamedPipe<unsigned char> &pipe(uint64_t pipeNum) const;

public:
    DASHAdapt(const DASHAdapt &) = delete;
    DASHAdapt &operator=(const DASHAdapt &) = delete;
    DASHAdapt(DASHAdapt &&)                 = delete;
    DASHAdapt &operator=(DASHAdapt &&) = delete;

    /**
     * @brief Constructs a new DASHAdapt connected to the given pipes.
     *
     * @param pipes the pipes in which the media data will be written
     */
    explicit DASHAdapt(std::vector<std::reference_wrapper<NamedPipe<unsigned char>>> &pipes);

    /**
     * @brief Destructor, frees allocated resources.
     */
    ~DASHAdapt() override = default;

    /**
     * @brief Attaches a DASHReader to this DASHAdapt object.
     *
     * This method takes ownership of the reader.
     * @param pReader the DASHReader to attach
     */
    void setReader(std::unique_ptr<DASHReader> pReader);

    /**
     * @brief Attaches a playback clock to this DASHAdapt object.
     *
     * This is clock is used to download enough data from the server, in order to prevent starvation or memory overload.
     * @param pClock a pointer to the clock
     */
    void attachClock(std::shared_ptr<Clock> &pClock);
};
