#include "Segment.hpp"

Segment::Segment(dash::mpd::ISegment &initSeg)
    : mpDownloadObsv(std::make_unique<DownloadObserver>()),
      mSeg(initSeg),
      mCursor(0)
{
    mSeg.AttachDownloadObserver(mpDownloadObsv.get());
}

std::vector<unsigned char>::iterator Segment::read(uint64_t len)
{
    return read(len, mCursor);
}

std::vector<unsigned char>::iterator Segment::read(uint64_t len, uint64_t origin)
{
    while (!mpDownloadObsv->isComplete())
        ;

    if (totalSize() == 0)
        throw std::runtime_error(dynamic_cast<dash::network::IChunk &>(mSeg).AbsoluteURI());

    if (len == 0)
        len = totalSize();    // read the whole segment if no size specified

    mCursor += len;

    if (mExtractedData.size() < (origin + len))
    {
        len      = origin + len - mExtractedData.size();
        auto tmp = std::make_unique<unsigned char[]>(static_cast<size_t>(len));
        mSeg.Read(tmp.get(), static_cast<size_t>(len));
        mExtractedData.insert(mExtractedData.end(), tmp.get(), tmp.get() + len);
    }

    return mExtractedData.begin() + static_cast<int64_t>(origin);
}

uint64_t Segment::totalSize() const
{
    return mpDownloadObsv->totalSize();
}

uint64_t Segment::downloadSpeed() const
{
    return mpDownloadObsv->averageDLSpeedBps();
}

bool Segment::startDownload()
{
    return mSeg.StartDownload();
}

void Segment::stopDownload()
{
    mSeg.AbortDownload();
}
