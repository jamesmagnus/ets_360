/**
 * @file Clock.hpp
 * @author Jean-Loup Beaussart
 * @class Clock
 * @brief Represents a simple playback clock.
 *
 * It's a simple clock in milliseconds with support to convert PTS (and DTS) from their timebase to ms.
 */

#pragma once

#include <boost/rational.hpp>
#include <chrono>
#include <mutex>

#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable : 4146)
#endif

extern "C" {
#include <libavutil/rational.h>
}

#ifdef _WIN32
#pragma warning(pop)
#endif

class Clock {
    using Clk = std::chrono::high_resolution_clock;

private:
    const boost::rational<int64_t> mcOneMs;
    boost::rational<int64_t> mVideoTimebaseMs;
    boost::rational<int64_t> mAudioTimebaseMs;
    mutable std::mutex mMutex;
    std::chrono::time_point<Clk> mOrigin;

    boost::rational<int64_t> convertTimebaseToMs(AVRational tb);

public:
    /**
     * @brief Deleted copy constructor.
     */
    Clock(const Clock &) = delete;

    /**
     * @brief Deleted assignment operator.
     */
    Clock &operator=(const Clock &) = delete;

    Clock(Clock &&) = delete;

    Clock &operator=(Clock &&) = delete;

    /**
     * @brief Constructs a new clock.
     */
    Clock();

    /**
     * @brief Constructs a new clock.
     *
     * @param tbVideo the time base of the video stream
     * @param tbAudio the time base of the audio stream
     */
    Clock(AVRational tbVideo, AVRational tbAudio);

    ~Clock() = default;

    /**
     * @brief Sets the video timebase for pts conversion.
     *
     * Do not change it when other threads are already using videoPtsToMs().
     * @param tb the timebase of pts ticks
     */
    void setVideoTimebase(AVRational tb);

    /**
     * @brief Sets the audio timebase for pts conversion.
     *
     * Do not change it when other threads are already using audioPtsToMs().
     * @param tb the timebase of pts ticks
     */
    void setAudioTimebase(AVRational tb);

    /**
     * @brief Gets the current clock value.
     *
     * @return the current clock value in milliseconds
     */
    std::chrono::milliseconds timeInMs() const;

    /**
     * @brief Converts a video pts in milliseconds.
     *
     * It also works with dts.
     * @param pts the pts to convert
     * @return the pts value in milliseconds
     */
    std::chrono::milliseconds videoPtsToMs(int64_t pts) const;

    /**
     * @brief Converts an audio pts in milliseconds.
     *
     * It also works with dts.
     * @param pts the pts to convert
     * @return the pts value in milliseconds
     */
    std::chrono::milliseconds audioPtsToMs(int64_t pts) const;

    /**
     * @brief Resets the clock to 0ms.
     *
     * @return the value of the clock before resetting
     */
    std::chrono::milliseconds reset();
};
