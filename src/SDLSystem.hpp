/**
 * @file SDLSystem.hpp
 * @author Jean-Loup Beaussart
 * @class SDL::System
 * @brief OOP wrapper for SDL window and renderer.
 *
 * This class provides convenient methods to setup SDL window, renderer and audio subsystem.
 * It allows creation of new textures and feeds data to the audio driver.
 */

#pragma once

#include <SDL.h>
#include <memory>
#include <string>

class Sample;
class Overlay;

namespace SDL {
class Texture;
class Rect;

class System {
private:
    std::unique_ptr<SDL_Window, void (*)(SDL_Window *)> mpWin;
    std::unique_ptr<SDL_Renderer, void (*)(SDL_Renderer *)> mpRenderer;
    std::unique_ptr<::Overlay> mpOverlay;
    int mLogicalWidth;
    int mLogicalHeight;
    SDL_AudioDeviceID mAudioDevice;

    void findWindowSize(int *pVideoW, int *pVideoH);

public:
    System(const System &) = delete;
    System &operator=(const System &) = delete;
    System(System &&)                 = delete;
    System &operator=(System &&) = delete;
    System(Uint32 flags, std::string const &winName, int w, int h);

    ~System();

    SDL_AudioDeviceID setupAudio(int sampleRate);

    void setOverlay(std::unique_ptr<::Overlay> pOverlay);

    Texture newTexture(Uint32 pixFormat, int access, int w = 0, int h = 0) const;
    Texture newTexture(std::unique_ptr<SDL_Surface, void (*)(SDL_Surface *)> pSurface) const;

    void renderClear() const;

    void renderPresent() const;

    void renderCopy(Texture &tex, Rect &rectSrc, Rect &rectDst) const;

    void renderCopy(Texture &tex) const;

    int renderWidth() const;

    int renderHeight() const;

    void feedAudio(std::unique_ptr<Sample> pSample) const;

    static std::string printSDLFailure(std::string const &msg);
};
}    // namespace SDL
