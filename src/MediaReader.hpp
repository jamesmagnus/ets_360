/**
 * @file MediaReader.hpp
 * @author Jean-Loup Beaussart
 * @class MediaReader
 * @brief A threaded video file reader.
 *
 * This class is used to open a video file and get information about duration, streams, codecs, ...
 * It is also responsible for reading the whole file into paquets.
 * The packets are read inside a thread and put into the correct output queue to go to the decoder thread.
 */

#pragma once

#include "Packet.hpp"
#include "ThreadBase.hpp"
#include "VideoData.hpp"
#include <atomic>

extern "C" {
#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable : 4244)
#endif

#include <libavformat/avformat.h>
#include <libavutil/rational.h>

#ifdef _WIN32
#pragma warning(pop)
#endif
}

struct AVFormatContext;
struct AVCodecContext;
struct AVCodec;
class AudioDecoder;
class VideoDecoder;
class Clock;

template <class T>
class ConcurrentQueue;

class MediaReader : public ThreadBase {
private:
    static const int scErrBuffSize;
    static const std::chrono::milliseconds scPacketBufferTime;
    std::unique_ptr<AVFormatContext, void (*)(AVFormatContext *)> mpFormatCtx;
    std::shared_ptr<AVCodecContext> mpVideoCodecCtx;
    std::shared_ptr<AVCodecContext> mpAudioCodecCtx;
    AVCodec *mpVideoCodec;
    AVCodec *mpAudioCodec;
    AudioDecoder *mpAudioDecThread;
    VideoDecoder *mpVideoDecThread;
    int mErrCde;
    int mVideoStream;
    int mAudioStream;
    std::atomic_bool mStop;
    std::unique_ptr<ConcurrentQueue<Packet>> &mpVideoQ;
    std::unique_ptr<ConcurrentQueue<Packet>> &mpAudioQ;
    std::shared_ptr<const Clock> mpcClock;

    [[noreturn]] void throwAVLibError() const;
    [[noreturn]] void throwError(std::string const &msg) const;
    bool threadBegin() override;
    bool threadWork() override;
    void threadHandleMsg(const ThreadMessageBase &msg) override;
    void threadEnd() override;
    bool processNextPackets();
    void setupCodecs(bool hwAccel = true);
    AVCodec *findCodecAuto(AVCodecParameters *codecParams) const;
    AVCodec *findCodecByName(const std::string &name) const;
    void streamComponentOpen();
    void sleepUntilDecodeTime(std::chrono::milliseconds dts);

public:
    MediaReader(const MediaReader &) = delete;
    MediaReader &operator=(const MediaReader &) = delete;
    MediaReader(MediaReader &&)                 = delete;
    MediaReader &operator=(MediaReader &&) = delete;
    MediaReader(std::string const &file, std::unique_ptr<ConcurrentQueue<Packet>> &pVideoQ, std::unique_ptr<ConcurrentQueue<Packet>> &pAudioQ, const VideoData &videoData);

    ~MediaReader() override = default;

    int videoWidth() const;

    int videoHeight() const;

    AVRational audioTimebase() const;

    AVRational videoTimebase() const;

    void attachAudioDecoder(AudioDecoder &audioDec);

    void attachVideoDecoder(VideoDecoder &videoDec);

    void attachPlaybackClock(std::shared_ptr<const Clock> pcClk);

    std::shared_ptr<AVCodecContext> audioCtx() const;

    std::shared_ptr<AVCodecContext> videoCtx() const;

    bool hasAudio() const;
};
