#include "Packet.hpp"

extern "C" {
#ifdef _WIN32
#pragma warning(push)
#pragma warning(disable : 4244)
#endif

#include <libavcodec/avcodec.h>

#ifdef _WIN32
#pragma warning(pop)
#endif
}

Packet::Packet()
    : mpPacket(av_packet_alloc(), [](AVPacket *p) { av_packet_free(&p); })
{
    if (!mpPacket)
        throw std::bad_alloc();
}

size_t Packet::size() const
{
    return static_cast<size_t>(mpPacket->size);
}

int64_t Packet::dts() const
{
    return mpPacket->dts;
}

const unique_ptr_AVPacket &Packet::avPacket() const
{
    return mpPacket;
}
