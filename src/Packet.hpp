/**
 * @file Packet.hpp
 * @author Jean-Loup Beaussart
 * @class Packet
 * @brief This class encapsulates an AVPacket
 *
 * It is also responsible for managing the associated memory buffers.
 */

#pragma once

#include <memory>

struct AVPacket;

using unique_ptr_AVPacket = std::unique_ptr<AVPacket, void (*)(AVPacket *)>;

class Packet {
private:
    unique_ptr_AVPacket mpPacket;

public:
    Packet(const Packet &) = delete;
    Packet &operator=(const Packet &) = delete;
    Packet(Packet &&)                 = delete;
    Packet &operator=(Packet &&) = delete;
    Packet();

    ~Packet() = default;

    size_t size() const;

    int64_t dts() const;

    const unique_ptr_AVPacket &avPacket() const;
};
