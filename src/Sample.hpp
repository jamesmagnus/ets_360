/**
 * @file Sample.hpp
 * @author Jean-Loup Beaussart
 * @class Sample
 * @brief Class representing a group of audio samples
 *
 * This class holds audio samples and PTS for sync purpose.
 */

#pragma once

#include <memory>

class Sample {
private:
    std::unique_ptr<unsigned char *[], void (*)(unsigned char **)> mppBuffer;
    size_t mBufferSize;
    int mNbSamples;
    const int64_t mcPTS;

public:
    Sample(const Sample &) = delete;
    Sample &operator=(const Sample &) = delete;
    Sample(Sample &&)                 = delete;
    Sample &operator=(Sample &&) = delete;
    Sample(size_t size, int64_t pts);

    ~Sample() = default;

    unsigned char **raw() const;

    void setNbSamples(int n);

    void setSize(size_t size);

    size_t size() const;

    int nbSamples() const;

    int64_t pts() const;
};
