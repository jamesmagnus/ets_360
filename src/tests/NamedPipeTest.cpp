#include "NamedPipe.hpp"
#include <catch2/catch.hpp>
#include <iostream>
#include <random>
#include <thread>

void generateData();
void writer();
void reader();

static const int scBlocSize = 1024;
static const int scNbBloc   = 256;
static const int scSize     = scNbBloc * scBlocSize;
static std::vector<int> sData(scSize);
static NamedPipe<int> sPipe;
static std::string sPipeName = "test_name";

void generateData()
{
    std::random_device rd;
    std::mt19937 engine{rd()};
    std::uniform_int_distribution<int> dist{-1000, 1000};

    auto gen = [&dist, &engine]() {
        return dist(engine);
    };

    std::generate(sData.begin(), sData.end(), gen);
}

void writer()
{
    for (auto bloc = 0; bloc < scNbBloc; ++bloc)
    {
        auto pData = std::make_unique<std::vector<int>>(sData.begin() + bloc * scBlocSize, sData.begin() + (bloc + 1) * scBlocSize);
        sPipe.setInputData(std::move(pData));
    }
    sPipe.setInputData(nullptr);
    sPipe.waitThread();
}

void reader()
{
    std::ifstream pipeStream;
    pipeStream.open(sPipeName, std::ios::binary);

    REQUIRE(pipeStream.is_open());
    REQUIRE(pipeStream.good());

    for (const int cValueReference : sData)
    {
        int v = 0;
        pipeStream.read(reinterpret_cast<char *>(&v), sizeof(v));
        INFO("READER: expected " << cValueReference << " but read " << v);
        REQUIRE(v == cValueReference);
    }

    pipeStream.close();
}

TEST_CASE("Test read/write with 2 threads")
{
    generateData();
    sPipeName = sPipe.create(sPipeName);

    std::thread w(writer);
    std::thread r(reader);

    sPipe.waitThread();
    w.join();
    r.join();
}
