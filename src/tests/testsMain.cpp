#define CATCH_CONFIG_RUNNER
#define CATCH_CONFIG_NO_CPP17_UNCAUGHT_EXCEPTIONS
#include <catch2/catch.hpp>

#define LOGURU_IMPLEMENTATION 1
#include "LoguruLogger.hpp"

int main(int argc, char *argv[])
{
    loguru::set_fatal_handler([](const loguru::Message &message) {
        throw std::runtime_error(std::string(message.prefix) + message.message);
    });

    loguru::g_stderr_verbosity = loguru::Verbosity_OFF;

    int result = Catch::Session().run(argc, argv);

    return result;
}
