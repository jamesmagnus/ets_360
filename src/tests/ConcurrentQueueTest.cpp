#include "ConcurrentQueue.hpp"
#include <catch2/catch.hpp>
#include <thread>

void addNumbers();
void getAndCheck();

struct Number {
    int mV;
    uint32_t mSize;

    uint32_t size()
    {
        return mSize;
    }
};

static const int MAX = 1000000;
static ConcurrentQueue<Number> Q;

TEST_CASE("Test new queue")
{
    ConcurrentQueue<Number> q;
    REQUIRE(q.size() == 0);
    REQUIRE_FALSE(q.isFlushed());
    REQUIRE(q.nbElements() == 0);
}

TEST_CASE("Test tryPut() on empty queue")
{
    ConcurrentQueue<Number> q;

    auto pN   = std::make_unique<Number>();
    pN->mSize = sizeof(int);

    REQUIRE(q.tryPut(std::move(pN)));
    REQUIRE(q.size() == sizeof(int));
    REQUIRE(q.nbElements() == 1);
    REQUIRE_FALSE(q.isFlushed());
}

TEST_CASE("Test get() on empty queue")
{
    using namespace std::chrono_literals;

    ConcurrentQueue<Number> q;

    auto nullRes = q.get(50ms);

    REQUIRE_FALSE(nullRes);
    REQUIRE(q.size() == 0);
    REQUIRE(q.nbElements() == 0);
    REQUIRE_FALSE(q.isFlushed());
}

TEST_CASE("Test tryPut() then get()")
{
    ConcurrentQueue<Number> q;

    {
        auto pN   = std::make_unique<Number>();
        pN->mSize = sizeof(int);
        pN->mV    = 42;

        REQUIRE(q.tryPut(std::move(pN)));

        pN        = std::make_unique<Number>();
        pN->mSize = sizeof(int);
        pN->mV    = -24;

        REQUIRE(q.tryPut(std::move(pN)));
        REQUIRE(q.nbElements() == 2);
        REQUIRE(q.size() == q.nbElements() * sizeof(int));
    }
    {
        auto res = q.get();

        REQUIRE(res);
        REQUIRE(res->mV == 42);
        REQUIRE(res->mSize == sizeof(int));
        REQUIRE(q.nbElements() == 1);
        REQUIRE(q.size() == q.nbElements() * sizeof(int));

        res = q.get();

        REQUIRE(res);
        REQUIRE(res->mV == -24);
        REQUIRE(res->mSize == sizeof(int));
        REQUIRE(q.nbElements() == 0);
        REQUIRE(q.size() == q.nbElements() * sizeof(int));
    }
    REQUIRE_FALSE(q.isFlushed());
}

TEST_CASE("Test flushing queue")
{
    ConcurrentQueue<Number> q;

    auto pN   = std::make_unique<Number>();
    pN->mSize = sizeof(int);
    pN->mV    = 42;
    q.tryPut(std::move(pN));
    q.flush();

    REQUIRE(q.isFlushed());
    REQUIRE(q.nbElements() == 1);
    REQUIRE(q.size() == q.nbElements() * sizeof(int));
}

TEST_CASE("Test get() on flushed queue")
{
    ConcurrentQueue<Number> q;

    auto pN   = std::make_unique<Number>();
    pN->mSize = sizeof(int);
    pN->mV    = 42;
    q.tryPut(std::move(pN));
    q.flush();

    auto res = q.get();

    REQUIRE(q.isFlushed());
    REQUIRE(q.nbElements() == 0);
    REQUIRE(q.size() == q.nbElements() * sizeof(int));
    REQUIRE(res);
    REQUIRE(res->size() == sizeof(int));
    REQUIRE(res->mV == 42);

    res = q.get();

    REQUIRE_FALSE(res);
}

TEST_CASE("Test tryPut() on flushed queue")
{
    ConcurrentQueue<Number> q;
    auto pN   = std::make_unique<Number>();
    pN->mSize = sizeof(int);
    pN->mV    = 42;
    q.flush();

    REQUIRE_FALSE(q.tryPut(std::move(pN)));
    REQUIRE(q.isFlushed());
    REQUIRE(q.nbElements() == 0);
    REQUIRE(q.size() == q.nbElements() * sizeof(int));
}

TEST_CASE("Test cleaning queue")
{
    ConcurrentQueue<Number> q;

    auto pN   = std::make_unique<Number>();
    pN->mSize = sizeof(int);
    pN->mV    = 42;
    q.tryPut(std::move(pN));

    pN        = std::make_unique<Number>();
    pN->mSize = sizeof(int);
    pN->mV    = -13;
    q.tryPut(std::move(pN));

    q.flush();

    REQUIRE(q.isFlushed());
    REQUIRE(q.nbElements() == 2);
    REQUIRE(q.size() == q.nbElements() * sizeof(int));

    q.discardAllData();

    REQUIRE(q.isFlushed());
    REQUIRE(q.nbElements() == 0);
    REQUIRE(q.size() == q.nbElements() * sizeof(int));
}

TEST_CASE("Test cleaning non flushed queue throws")
{
    ConcurrentQueue<Number> q;

    auto pN   = std::make_unique<Number>();
    pN->mSize = sizeof(int);
    pN->mV    = 42;
    q.tryPut(std::move(pN));

    pN        = std::make_unique<Number>();
    pN->mSize = sizeof(int);
    pN->mV    = -13;
    q.tryPut(std::move(pN));

    REQUIRE_FALSE(q.isFlushed());
    REQUIRE(q.nbElements() == 2);
    REQUIRE(q.size() == q.nbElements() * sizeof(int));

    REQUIRE_THROWS(q.discardAllData());
}

void addNumbers()
{
    for (int i = 0; i < MAX; ++i)
    {
        auto pN   = std::make_unique<Number>();
        pN->mSize = sizeof(int);
        pN->mV    = i;

        REQUIRE(Q.tryPut(std::move(pN)));
        REQUIRE((Q.size() % sizeof(int)) == 0);
        REQUIRE_FALSE(Q.isFlushed());
    }
}

void getAndCheck()
{
    for (int i = 0; i < MAX; ++i)
    {
        auto pN = Q.get();

        REQUIRE(pN->mV == i);
        REQUIRE((Q.size() % sizeof(int)) == 0);
        REQUIRE_FALSE(Q.isFlushed());
    }
}

TEST_CASE("Test concurrent calls to tryPut() and get()")
{
    std::thread producer(addNumbers);
    std::thread consumer(getAndCheck);

    producer.join();
    consumer.join();
}
