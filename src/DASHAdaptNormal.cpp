#include "DASHAdaptNormal.hpp"
#include "DASHReader.hpp"
#include "LoguruLogger.hpp"
#include "NamedPipe.hpp"

DASHAdaptNormal::DASHAdaptNormal(std::vector<std::reference_wrapper<NamedPipe<unsigned char>>> &pipes)
    : DASHAdapt(pipes)
{
    LOG_IF_S(WARNING, pipes.size() != 1) << "Normal DASH adapter is for non tiled video, it uses only one input pipe";
}

bool DASHAdaptNormal::logic()
{
    uint32_t n = reader().nextSegmentNumber() - 1;
    std::chrono::milliseconds expectedPTS(static_cast<int64_t>(n * reader().segmentDuration()));

    LOG_S(INFO) << "Next segment is expected for " << expectedPTS.count() << "ms";

    auto representations    = reader().listRepresentations();
    auto availableBandwidth = reader().lastBandwidthMeasure();
    auto greatestThatFit    = representations.lower_bound(availableBandwidth);

    // Value returned by lower_bound() is the first >= availableBandwidth, so we take the representation just before it
    if (greatestThatFit != representations.cbegin())
        greatestThatFit--;

    LOG_S(INFO) << "Last network measure is " << availableBandwidth << " bps (" << availableBandwidth / 8192 << " kBps)";

    LOG_S(INFO) << "Selecting representation '" << greatestThatFit->second << "' with bitrate " << greatestThatFit->first << "(" << greatestThatFit->first / 8192 << "kBps)";

    try
    {
        reader().changeRepresentation(greatestThatFit->first);
        reader().downloadNextSegment();

        auto pData = reader().nextSegmentData();
        if (!pData)
            return false;

        pipe(0).setInputData(std::move(pData));
    }
    catch (std::exception &e)
    {
        LOG_S(ERROR) << "Cannot download segment: " << e.what();
    }

    return true;
}

void DASHAdaptNormal::initLogic()
{
    reader().minQ();
}
