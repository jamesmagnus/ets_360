/**
 * @file Sync.hpp
 * @author Jean-Loup Beaussart
 * @class Sync
 * @brief This class handles syncing video and audio and displaying.
 *
 * It extracts raw video frames (YUV) and raw audio samples (Sample) from the queues.
 * Then it syncs them according to their PTS.
 * Audio syncing runs in its own thread.
 * Video syncing is a loop that should be run in the main thread because SDL calls aren't thread safe. 
 */

#pragma once

#include "SDLTexture.hpp"
#include "Sample.hpp"
#include "ThreadBase.hpp"
#include "VideoData.hpp"
#include "Viewport.hpp"
#include "YUV.hpp"
#include <atomic>
#include <vector>

class Clock;
namespace SDL {
class System;
}    // namespace SDL

template <class T>
class ConcurrentQueue;

class Sync : public ThreadBase {
private:
    const std::chrono::milliseconds mcSyncThreshold;
    std::shared_ptr<const Clock> mpcClock;
    VideoData mVideoData;
    std::vector<std::unique_ptr<ConcurrentQueue<YUV>>> &mpYUVQs;
    std::vector<std::unique_ptr<YUV>> mpLastFrame;
    std::unique_ptr<ConcurrentQueue<Sample>> &mpSampleQ;
    std::unique_ptr<SDL::System> &mpSDLSys;
    Viewport mViewport;
    std::vector<SDL::Texture> mSDLScreens;
    std::atomic_bool mQuit;

    bool threadBegin() override;
    bool threadWork() override;
    void threadHandleMsg(const ThreadMessageBase &msg) override;
    void threadEnd() override;
    void updateTile(std::unique_ptr<YUV> &pFrame, uint64_t tile);
    int64_t pullFrameTiles();
    void syncAndDisplay(std::chrono::milliseconds pts);
    void flushAllQs();
    void discardAllQs();
    bool areQsFlushed() const;
    static bool areAllTrue(const std::vector<bool> &bools);

public:
    Sync(const Sync &) = delete;
    Sync &operator=(const Sync &) = delete;
    Sync(Sync &&)                 = delete;
    Sync &operator=(Sync &&) = delete;

    /**
     * @brief test doc for call graph
     */
    Sync(std::unique_ptr<SDL::System> &pSDLSystem, std::vector<std::unique_ptr<ConcurrentQueue<YUV>>> &pVideoQs, std::unique_ptr<ConcurrentQueue<Sample>> &pAudioQ, const VideoData &cVideoData);

    ~Sync() override = default;

    void attachPlaybackClock(std::shared_ptr<const Clock> pcClock);

    void updateLoop();
};
